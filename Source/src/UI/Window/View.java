package UI.Window;

import UI.Events.IMenuListener;
import UI.Util.UISize;
import UI.Window.Menu.*;
import UI.Window.Menu.Menu;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Created by deke on 06.05.2015.
 */
public class View extends JFrame{

//    protected final static JMenuBar menuBar = View.initMenuBar();

    protected JPanel contentPane;
    private IMenuListener listener;

    public IMenu menu;

    public View()
    {
        super("Dots and Boxes");




        Rectangle r = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
        int w = (int) r.getWidth();
        int h = (int) r.getHeight();


        int x = w/2 -(UISize.screenWidth/2);
        int y = h/2 -(UISize.screenHeight/2)-40;    //-40 for higher Location than the middle

        this.setLocation(x, y);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.menu = new Menu(this,this.listener);

        this.add((Component) this.menu, BorderLayout.NORTH);


    }

    public void registerMenuEvents(IMenuListener listener)
    {
        this.listener = listener;
        this.menu.registerMenuEvents(listener);
    }





}
