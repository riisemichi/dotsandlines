package UI.Window.GameView.Board;

import UI.Events.ILinePressedListener;
import UI.Window.GameView.Component.Actor.HLine;
import UI.Window.GameView.Component.Actor.VLine;
import UI.Window.GameView.Component.Field.Corner;
import UI.Window.GameView.Component.Field.Field;
import UI.Util.UISize;

import javax.swing.*;
import java.awt.*;

/**
 * Created by deke on 28.04.2015.
 */
public class GameBoard extends JPanel {
    private int width;
    private int height;
    private ILinePressedListener listener;


    public GameBoard(int width, int height,ILinePressedListener listener)
    {
        super();


        this.width = width;
        this.height = height;

        this.listener = listener;

        UISize.setXWidth(width);
        UISize.setYWidth(height);

        this.setBackground(Color.WHITE);
        this.setLayout(null);
        createBoard();


    }

    private void createBoard() {

        int width = (this.width) + (this.width + 1);
        int height = (this.height) + (this.height + 1);


        //Where the magic happens

        for (int r = 0; r < height; r++) {
            for (int c = 0; c < width; c++) {
                //Corner and Lines
                if (r % 2 == 0) {
                    if (c % 2 == 0)  //Corner
                    {
                        this.add(new Corner(c, r));
                    } else        //Horizontal Line
                    {
                        if (c != (width)) {
                            this.add(new HLine(c, r,this.listener));
                        }
                    }
                } else    //Lines and Fields
                {
                    if (c % 2 == 0)  //Vertical Line
                    {
                        this.add(new VLine(c, r,this.listener));
                    } else        //Field
                    {
                        if (c != (width)) {
                            this.add(new Field(c, r));
                        }
                    }
                }
            }
        }


        this.setSize(UISize.screenWidth, UISize.screenHeight);
        this.setPreferredSize(new Dimension(UISize.screenWidth, UISize.screenHeight));
    }
}
