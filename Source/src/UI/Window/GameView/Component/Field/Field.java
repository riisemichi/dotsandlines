package UI.Window.GameView.Component.Field;

import UI.Util.UISize;
import UI.Window.GameView.GameView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by deke on 30.04.2015.
 */
public class Field extends JPanel{
    protected Point point;

    public Field(int x,int y)
    {
        super();
        this.setBackground(Color.WHITE);
        this.setBounds((UISize.xWidth * (x * 2 - 1)) + UISize.xOffset, (UISize.yWidth * (y * 2 - 1)) + UISize.yOffset, (UISize.xWidth * 3), (UISize.yWidth * 3));

        this.setPoint(x, y);
        GameView.fields.put(getPoint(), this);
    }

    private void setPoint(int x,int y) {
        this.point = new Point((x-1)/2,(y-1)/2);
    }


    /**
     * Returns upper left Corner as a Point
     * @return
     */
    public Point getPoint()
    {
        return this.point;
    }
}
