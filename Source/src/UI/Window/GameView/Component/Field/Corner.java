package UI.Window.GameView.Component.Field;

import UI.Util.UISize;

import javax.swing.*;
import java.awt.*;

/**
 * Created by deke on 30.04.2015.
 */
public class Corner extends JPanel {
    public Corner(int x, int y)
    {
        super();
        this.setBackground(Color.BLACK);
        this.setBorder(BorderFactory.createLineBorder(Color.WHITE));
        this.setBounds((UISize.xWidth*x*2)+UISize.xOffset, (UISize.yWidth*y*2)+UISize.yOffset, UISize.xWidth, UISize.yWidth);

    }
}
