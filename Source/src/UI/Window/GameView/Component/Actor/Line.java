package UI.Window.GameView.Component.Actor;

import Logic.GameLogicFactory;
import Logic.IMove;
import Logic.Move;
import UI.Events.ILinePressedListener;
import UI.Window.GameView.GameView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by deke on 30.04.2015.
 */
public abstract class Line extends JButton implements ActionListener{

    protected Point startPoint;
    protected Point endPoint;
    private ILinePressedListener listener;

    public Line(int x,int y, ILinePressedListener listener)
    {
        super();
        this.listener = listener;
        this.setPoints(x,y);
        this.setBackground(Color.WHITE);
        this.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        this.addActionListener(this);

        //Save the Buttons in a HashMap for the GameView
        //Move move = new Move(this.getStartPoint(),this.getEndPoint());

        //
        String p1x = String.valueOf((int) getStartPoint().getX());
        String p1y = String.valueOf((int)getStartPoint().getY());
        String p2x = String.valueOf((int)getEndPoint().getX());
        String p2y = String.valueOf((int)getEndPoint().getY());

        String p = p1x+p1y+p2x+p2y;

        IMove move = GameLogicFactory.getInstance().createMove(this.getStartPoint(),this.getEndPoint());
        GameView.lines.put(move,this);
        //GameView.lines.put(p,this);
    }

    protected Point getStartPoint() {
       return this.startPoint;
    }

    protected Point getEndPoint() {
        return this.endPoint;
    }

    protected abstract void setPoints(int x,int y);



    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Point p1 = getStartPoint();
        Point p2 = getEndPoint();
        this.listener.linePressed(p1,p2);
    }
}
