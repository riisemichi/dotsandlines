package UI.Window.GameView.Component.Actor;

import Logic.Move;
import UI.Events.ILinePressedListener;
import UI.Util.UISize;
import UI.Window.GameView.GameView;

import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Created by deke on 01.05.2015.
 */
public class HLine extends Line {


    public HLine(int x,int y,ILinePressedListener listener){
        super(x,y,listener);
        this.setBounds((UISize.xWidth * (x*2-1))+UISize.xOffset, (UISize.yWidth * y*2)+UISize.yOffset, (UISize.xWidth * 3), (UISize.yWidth));
    }




    @Override
    protected void setPoints(int x, int y) {
        this.startPoint = new Point((x-1)/2,y/2);
        this.endPoint = new Point(((x-1)/2)+1,y/2);
    }

}
