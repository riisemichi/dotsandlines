package UI.Window.GameView.Component.Actor;

import Logic.Move;
import UI.Events.ILinePressedListener;
import UI.Util.UISize;
import UI.Window.GameView.GameView;

import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Created by deke on 01.05.2015.
 */
public class VLine extends Line {
    public VLine(int x,int y,ILinePressedListener listener){
        super(x,y,listener);
        this.setBounds((UISize.xWidth * x*2)+UISize.xOffset, (UISize.yWidth * (y*2-1))+UISize.yOffset, (UISize.xWidth), (UISize.yWidth * 3));
    }



    @Override
    protected void setPoints(int x, int y) {
        this.startPoint = new Point(x/2,(y-1)/2);
        this.endPoint = new Point(x/2,((y-1)/2)+1);
    }


}
