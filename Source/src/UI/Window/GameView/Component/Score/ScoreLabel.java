package UI.Window.GameView.Component.Score;

import java.awt.*;


/**
 * Created by deke on 13.05.2015.
 */
public class ScoreLabel extends Label {
    public ScoreLabel(String str)
    {
        super(str);
        this.setBackground(Color.WHITE);
        this.setAlignment(CENTER);
        this.setFont(new Font("Serif", Font.PLAIN, 30));
    }
}
