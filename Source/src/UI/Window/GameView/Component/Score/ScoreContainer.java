package UI.Window.GameView.Component.Score;

import Logic.IPlayer;
import UI.Window.GameView.GameView;

import javax.swing.*;
import java.awt.*;

/**
 * Created by deke on 17.05.2015.
 */
public class ScoreContainer extends JPanel {



    public ScoreContainer()
    {
        super();
        this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
    }

    public void setPlayers(IPlayer[] players)
    {
        //Clear Container
        this.removeAll();

        //Set cells for the ScoreContainer
        this.setLayout(new GridLayout(1, players.length * 2));

        //to be attached into the ScoreContainer
        ScoreLabel name;
        ScoreLabel score;

        for(int i=0;i<players.length;i++)
        {
            //Players Name
            name = new ScoreLabel(players[i].getName());
            name.setForeground(players[i].getColor());
            this.add(name);

            //Players Score
            score = new ScoreLabel("0");
            score.setForeground(players[i].getColor());
            this.add(score);

            //Register the score in the GameView, to be able to update the player's score
            GameView.scores.put(players[i],score);

        }
    }
}
