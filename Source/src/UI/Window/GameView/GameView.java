package UI.Window.GameView;

import Logic.*;
import UI.Events.IEndViewListener;
import UI.Events.ILinePressedListener;
import UI.IGameView;
import UI.Window.GameView.Board.GameBoard;
import UI.Window.GameView.Component.Actor.Line;
import UI.Window.GameView.Component.Field.Field;
import UI.Window.GameView.Component.Score.ScoreContainer;
import UI.Window.GameView.Component.Score.ScoreLabel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by deke on 13.05.2015.
 */
public class GameView extends UI.Window.View implements IGameView, ILinePressedListener{


    public static HashMap<IMove,Line> lines             = new HashMap<>();
    public static HashMap<Point,Field> fields          = new HashMap<>();
    public static HashMap<IPlayer,ScoreLabel> scores    = new HashMap<>();

    protected JPanel contentPane;

    public static IEndViewListener listener;

    protected int width;
    protected int height;

    private boolean viewCreated = false;

    public ArrayList<ILocalPlayer> localPlayers;

    protected final ScoreContainer scoreContainer = new ScoreContainer();

    protected ILinePressedListener linePressedListener;

    public GameView(int width, int height)
    {
        super();
        this.width = width;
        this.height = height;
        this.linePressedListener = this;
        this.localPlayers = new ArrayList<>();
    }
    


    private void initGameView()
    {
        JPanel jPanel = new JPanel(new BorderLayout());
        //ScoreView
        jPanel.add(this.scoreContainer,BorderLayout.NORTH);
        //=========
        //GameBoard
        GameBoard gameBoard = new GameBoard(this.width,this.height,this.linePressedListener);
        jPanel.add(gameBoard,BorderLayout.SOUTH);
        //===========

        this.contentPane = jPanel;
    }

    /**
     * Display the Game View.
     */
    @Override
    public void showView() {
        if (this.viewCreated == false)
        {
            this.viewCreated = true;
            this.initGameView();
            this.getContentPane().add(this.contentPane);
            this.pack();
        }

        this.setVisible(true);


    }

    /**
     * Stop displaying the Game View.
     */
    @Override
    public void hideView() {
        this.setVisible(false);
    }

    /**
     * Sets the players for displaying.
     *
     */
    @Override
    public void gameStarted(IPlayer[] players) {
        //Set the players Points and Names for the FIRST Time
        this.scoreContainer.setPlayers(players);

    }

    /**
     * Sets the player that is controlled by the UI.
     *
     * @param localPlayer
     */
    @Override
    public void setUIPlayer(ILocalPlayer localPlayer) {

        this.localPlayers.add(localPlayer);
    }

    /**
     * Set the list of Moves to display on the UI.
     *
     * @param moves
     */
    @Override
    public void setMoves(IPlayerMove[] moves) {
        for(int i =0;i<moves.length;i++)
        {
            Line line = GameView.lines.get(moves[i].getMove());
            line.setOpaque(true);
            line.setBackground(moves[i].getPlayer().getColor());
        }
    }

    /**
     * Set the list of boxes to display in the UI.
     *
     * @param boxes
     */
    @Override
    public void setBoxes(IBox[] boxes) {
        for(int i=0;i<boxes.length;i++)
        {
            Field field = GameView.fields.get(boxes[i].getPoint());
            field.setOpaque(true);
            field.setBackground(boxes[i].getPlayer().getColor());
        }
    }

    /**
     * Set the points of a player to display in the UI.
     *
     * @param player
     * @param pointsOfPlayer
     */
    @Override
    public void setPointsOfPlayer(IPlayer player, int pointsOfPlayer) {
        //Sets the Points into the Player's ScoreLabel
        GameView.scores.get(player).setText(String.valueOf(pointsOfPlayer));
    }

    @Override
    public void linePressed(Point p1, Point p2) {
        IMove move = GameLogicFactory.getInstance().createMove(p1, p2);
        for(ILocalPlayer player:this.localPlayers)
        {
            player.move(move);
        }

    }
}
