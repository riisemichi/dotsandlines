package UI.Window.EndView.Component;

import UI.Events.IEndViewListener;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by deke on 18.05.2015.
 */
public class PlayAgainButton extends JButton implements ActionListener {
    private IEndViewListener listener;

    public PlayAgainButton(IEndViewListener listener)
    {
        super("OK");
        this.addActionListener(this);
        this.listener = listener;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        this.listener.okSelected();
    }
}
