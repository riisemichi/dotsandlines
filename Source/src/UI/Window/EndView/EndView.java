package UI.Window.EndView;

import Logic.IPlayer;
import UI.Events.IEndViewListener;
import UI.IEndView;
import UI.Window.EndView.Component.PlayAgainButton;
import UI.Window.EndView.Component.QuitButton;
import UI.Window.View;

import javax.swing.*;
import java.awt.*;

/**
 * Created by deke on 18.05.2015.
 */
public class EndView extends View implements IEndView{

    private boolean viewCreated;

    private JPanel contentPane;

    private JLabel labelWon;
    private IEndViewListener listener;

    private JButton playAgain;
    private JButton quit;
    private JPanel buttonPanel;
    private JPanel container;

    public EndView()
    {
        super();
    }



    public void initEndView()
    {
        this.container = new JPanel();

        this.buttonPanel = new JPanel();
        this.labelWon = new JLabel();

        this.playAgain = new PlayAgainButton(this.listener);
        this.quit = new QuitButton(this.listener);

        this.buttonPanel.add(playAgain);
        this.buttonPanel.add(quit);

        container.add(labelWon,BorderLayout.NORTH);
        container.add(buttonPanel,BorderLayout.SOUTH);

        this.contentPane = container;

    }

    public void showView() {
        if (this.viewCreated == false)
        {
            this.viewCreated = true;
            this.initEndView();
            this.getContentPane().add(this.contentPane);
            this.pack();
        }

        this.setVisible(true);


    }

    /**
     * Stop displaying the Game View.
     */
    @Override
    public void hideView() {
        this.setVisible(false);
    }

    @Override
    public void setWinner(IPlayer winner) {
        this.labelWon.setText(winner.getName() + " won!");
        this.labelWon.setForeground(winner.getColor());
        this.pack();
    }

    /**
     * Register a listener to recieve Event notifications.
     *
     * @param listener the listener that wants to recieve event notifications.
     */
    @Override
    public void registerEndViewEvents(IEndViewListener listener) {
        this.listener = listener;
    }
}
