package UI.Window.Menu;

import Controller.BoardSizes;
import UI.Events.IMenuListener;
import UI.Window.View;

import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import javax.swing.filechooser.FileFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Created by deke on 19.05.2015.
 */
public class Menu extends JMenuBar implements IMenu {

    private final JMenuItem jmiSave;
    private final JMenuItem jmiLoad;
    private final JMenuItem jmiClose;
    private final JMenuItem jmiAbout;
    private final JMenu jmBoardSize;


    private View view;
    private IMenuListener listener;

    public Menu(View view, IMenuListener listener) {
        super();
        this.view = view;
        this.listener = listener;

        JMenuBar menuBar = this;
        JMenu jmFile = new JMenu("File");
        JMenu jmGame = new JMenu("Game");
        JMenu jmHelp = new JMenu("Help");

        this.jmiSave = new JMenuItem("Save");
        this.jmiLoad = new JMenuItem("Load");
        this.jmiClose = new JMenuItem("Close");
        this.jmiAbout = new JMenuItem("About");

        this.jmBoardSize = new JMenu("BoardSize");

        JMenuItem jmSize;

        BoardSizes[] sizes = BoardSizes.values();
        for(BoardSizes size:sizes)
        {
            jmSize = new JMenuItem(size.toString());
            jmSize.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    Menu.this.listener.boardSizeSelected(size);
                }
            });
            this.jmBoardSize.add(jmSize);

        }

        jmFile.add(jmiSave);
        jmFile.add(jmiLoad);

        jmGame.add(jmiClose);
        jmGame.add(jmBoardSize);

        jmHelp.add(jmiAbout);

        menuBar.add(jmFile);
        menuBar.add(jmGame);
        menuBar.add(jmHelp);


        JFileChooser fc = new JFileChooser();

        //Filter only shows *.dnl - Files
        fc.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File file) {
                if (file.isDirectory()) {
                    return true;
                }
                final String name = file.getName();
                return name.endsWith(".dnl");
            }

            @Override
            public String getDescription() {
                return "*.dnl";
            }
        });

        //File Menu Listener
        jmFile.addMenuListener(new MenuListener() {
            @Override
            public void menuSelected(MenuEvent menuEvent) {
                if (Menu.this.listener.canLoad()) {
                    Menu.this.jmiLoad.setEnabled(true);
                } else {
                    Menu.this.jmiLoad.setEnabled(false);
                }

                if (Menu.this.listener.canSave()) {
                    Menu.this.jmiSave.setEnabled(true);
                } else {
                    Menu.this.jmiSave.setEnabled(false);
                }
            }

            @Override
            public void menuDeselected(MenuEvent menuEvent) {

            }

            @Override
            public void menuCanceled(MenuEvent menuEvent) {
            }

        });

        //Enable/Disable setting the BoardSize
        jmGame.addMenuListener(new MenuListener() {
            @Override
            public void menuSelected(MenuEvent menuEvent) {
                if(Menu.this.listener.canSetBoardSize())
                {
                    Menu.this.jmBoardSize.setEnabled(true);
                }
                else
                {
                    Menu.this.jmBoardSize.setEnabled(false);
                }
            }

            @Override
            public void menuDeselected(MenuEvent menuEvent) {

            }

            @Override
            public void menuCanceled(MenuEvent menuEvent) {

            }
        });

        //Help Menu Listener
        jmiAbout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JOptionPane.showMessageDialog(Menu.this.view,
                        "Starting with an empty grid of dots, players take turns, adding a single horizontal or vertical line between two unjoined adjacent dots.\n" + "" +
                                "A player who completes the fourth side of a 1�1 box earns one point and takes another turn.\n" + "" +
                                "The game ends when no more lines can be placed. The winner of the game is the player with the most points.");
            }
        });
        //Load
        jmiLoad.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent actionEvent) {
                  int returnVal = fc.showOpenDialog(Menu.this.view);

                  if (returnVal == JFileChooser.APPROVE_OPTION) {
                      File file = fc.getSelectedFile();
                      Menu.this.listener.loadClicked(file);
                  }
              }
          }
        );
        //


        //Save Menu Listener
        jmiSave.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent actionEvent) {
                  int userSelection = fc.showSaveDialog(Menu.this.view);
                  if (userSelection == JFileChooser.APPROVE_OPTION) {
                      File file = fc.getSelectedFile();
                      Menu.this.listener.saveClicked(file);
                  }
              }
          }

        );
        //


        //Close Menu Listener
        jmiClose.addActionListener(new ActionListener() {
               @Override
               public void actionPerformed(ActionEvent actionEvent) {
                   Menu.this.listener.closeClicked();
               }
           }
        );
        //
    }


    @Override
    public void registerMenuEvents(IMenuListener listener) {
        Menu.this.listener = listener;
    }
}
