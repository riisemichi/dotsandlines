package UI.Window.Menu;

import UI.Events.IMenuListener;

/**
 * Created by deke on 19.05.2015.
 */
public interface IMenu {
    void registerMenuEvents(IMenuListener listener);
}
