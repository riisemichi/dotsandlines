package UI.Window.InvitationView;

import NetworkManagement.INetworkParticipant;
import UI.Events.IInvitationViewListener;
import UI.IInvitationView;
import UI.Window.EndView.EndView;
import UI.Window.InvitationView.Component.*;

import javax.swing.*;
import java.awt.*;

/**
 * Created by deke on 13.05.2015.
 */
public class InvitationView extends UI.Window.View implements IInvitationView {

    protected JPanel contentPane;

    protected ButtonLocaleGame btnLocalGame;
    protected ButtonLocaleGame btnAIGame;
    protected PlayerList playerList;
    public static IInvitationViewListener listener;
    private boolean viewCreated;

    


    public InvitationView()
    {
        super();
    }

    private void initInvitationWindow()
    {



        this.contentPane = new JPanel();
        this.contentPane.setLayout(new GridLayout(0,1));

        this.btnLocalGame = new ButtonLocaleGame("Player vs Player",false);
        this.btnAIGame = new ButtonLocaleGame("Player vs PC",true);


        JPanel btnPanel = new JPanel(new GridLayout(1,2));
        btnPanel.add(btnLocalGame);
        btnPanel.add(btnAIGame);

        this.contentPane.add(btnPanel);

        //My Name Label
        JLabel myName = new JLabel("My Name");
        myName.setHorizontalAlignment((int)CENTER_ALIGNMENT);
        myName.setFont(new Font("Serif", Font.PLAIN, 40));
        myName.setForeground(Color.BLACK);
        //

        //TextField for Name
        YourNameField txtYourName = new YourNameField();
        txtYourName.setHorizontalAlignment((int) CENTER_ALIGNMENT);
        //


        JPanel namePanel = new JPanel(new GridLayout(1,2));
        namePanel.add(myName);
        namePanel.add(txtYourName);

        this.contentPane.add(namePanel,BorderLayout.CENTER);

        this.playerList = new PlayerList(this.listener);
        this.contentPane.add(this.playerList);

        this.getContentPane().add(this.contentPane, BorderLayout.CENTER);
        this.setMinimumSize(new Dimension(800, 600));
        //Margin
        this.contentPane.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
        //
        this.setVisible(true);
    }


    /**
     * Display the InvitationView.
     */
    @Override
    public void showView() {
        if(this.viewCreated ==false)
        {
            this.initInvitationWindow();
            this.viewCreated = true;
        }
        else
        {
            this.setVisible(true);
        }

    }

    /**
     * Stop displaying the InvitationView.
     */
    @Override
    public void hideView() {
        this.setVisible(false);
    }

    /**
     * Register a listener to recieve Event notifications.
     *
     * @param listener the listener that wants to recieve event notifications.
     */
    @Override
    public void registerInvitationViewEvents(IInvitationViewListener listener)
    {
        InvitationView.listener = listener;
    }

    /**
     * Update the list of network Participants.
     *
     * @param participants to display.
     */
    @Override
    public void setParticipantList(INetworkParticipant[] participants) {
        this.playerList.setPlayers(participants);
        //Recalculate the Size for the Window with pack()
        this.pack();
    }


}
