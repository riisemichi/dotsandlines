package UI.Window.InvitationView.Component;

import UI.Window.InvitationView.InvitationView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by deke on 13.05.2015.
 */
public class ButtonLocaleGame extends JButton implements ActionListener {


    boolean versusAI;

    public ButtonLocaleGame(String text,boolean versusAI)
    {
        super(text);
        this.versusAI = versusAI;
        this.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent){

        if(this.versusAI)
        {
            InvitationView.listener.startAIGameSelected();
        }
        else
        {
            InvitationView.listener.startLocalGameSelected();
        }


    }
}


