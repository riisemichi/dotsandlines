package UI.Window.InvitationView.Component;

import NetworkManagement.INetworkParticipant;
import NetworkManagement.InvitationState;
import UI.Events.IInvitationViewListener;
import UI.IInvitationView;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by deke on 13.05.2015.
 */
public class PlayerList extends JPanel {

    private final GridLayout layout;
    private IInvitationViewListener listener;

    public PlayerList(IInvitationViewListener listener)
    {
        super();
        this.listener = listener;
        this.layout = new GridLayout(0, 1);
        this.setLayout(this.layout);
    }

    public void setPlayers(INetworkParticipant[] participants) {

        //Clear the Panel
        this.removeAll();
        //

        //Sort List
        ArrayList<INetworkParticipant> readyPats = new ArrayList<>();
        ArrayList<INetworkParticipant> otherPats = new ArrayList<>();


        for(INetworkParticipant participant:participants)
        {
            if(participant.getInvitationState() == InvitationState.ReadyToStart)
            {
                readyPats.add(participant);
            }
            else
            {
                otherPats.add(participant);
            }
        }

        //

        JPanel container = new JPanel(this.layout);

        //Add all READY Participants to the JPanel
        JPanel panelReady = new JPanel(this.layout);
        JPanel panelOther = new JPanel(this.layout);

        PlayerRow row;
        for(INetworkParticipant participant:readyPats)
        {
            row = new PlayerRow(participant,listener);
            panelReady.add(row);
        }



        for(INetworkParticipant participant:otherPats)
        {
            row = new PlayerRow(participant,listener);
            panelOther.add(row);
        }

        panelReady.setBorder(BorderFactory.createEmptyBorder(10,0,10,0));
        container.add(panelReady);
        container.add(panelOther);

        this.add(container);

    }
}
