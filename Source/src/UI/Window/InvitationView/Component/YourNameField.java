package UI.Window.InvitationView.Component;

import UI.Window.InvitationView.InvitationView;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by deke on 13.05.2015.
 */
public class YourNameField extends JTextField implements KeyListener {

    public YourNameField()
    {
        super();
        this.addKeyListener(this);
    }


    /**
     * Set Focus to the JTextField
     */
    @Override
    public void addNotify() {
        super.addNotify();
        requestFocus();
    }


    @Override
    public void keyTyped(KeyEvent keyEvent) {
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        InvitationView.listener.nameChanged(this.getText());
    }
}
