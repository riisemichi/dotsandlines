package UI.Window.InvitationView.Component;

import NetworkManagement.INetworkParticipant;
import UI.Events.IInvitationViewListener;

import javax.swing.*;
import java.awt.*;

/**
 * Created by deke on 13.05.2015.
 */
public class PlayerRow extends JPanel {

    protected Label lPlayer;
    protected InviteButton inviteButton;
    private IInvitationViewListener listener;


    public PlayerRow(INetworkParticipant participant,IInvitationViewListener listener)
    {
        super();
        this.listener = listener;
        this.setLayout(new GridLayout(1, 2));

        this.lPlayer = new PlayerLabel(participant.getName());
        this.inviteButton = new InviteButton(participant,listener);

        this.add(lPlayer);
        this.add(inviteButton);
    }
}
