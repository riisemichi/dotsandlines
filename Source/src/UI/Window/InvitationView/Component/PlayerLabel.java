package UI.Window.InvitationView.Component;

import javax.swing.*;
import java.awt.*;

/**
 * Created by deke on 13.05.2015.
 */
public class PlayerLabel extends Label {
    public PlayerLabel(String playerName)
    {
        super(playerName);
        //this.setBackground(Color.WHITE);
        this.setAlignment(CENTER);
        this.setFont(new Font("Serif", Font.PLAIN, 40));
        this.setForeground(Color.BLACK);

    }
}
