package UI.Window.InvitationView.Component;

import NetworkManagement.INetworkParticipant;
import NetworkManagement.InvitationState;
import UI.Events.IInvitationViewListener;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * Created by deke on 13.05.2015.
 */
public class InviteButton extends JButton implements ActionListener{


    protected InvitationState playerState;
    private IInvitationViewListener listener;
    private INetworkParticipant participant;

    public InviteButton(INetworkParticipant participant, IInvitationViewListener listener)
    {
        super();
        this.participant = participant;
        this.playerState = participant.getInvitationState();
        this.listener = listener;

        this.addActionListener(this);

        switch (this.playerState)
        {
            case None:
                this.setText("Invite");
                this.setOpaque(true);
                this.setBackground(Color.WHITE);
                this.setEnabled(true);
                break;
            case Invited:
                this.setText("Invited ...");
                this.setOpaque(true);
                this.setBackground(Color.WHITE);
                this.setEnabled(false);
                break;
            case ReadyToStart:
                this.setText("Play");
                this.setOpaque(true);
                this.setBackground(Color.GREEN);
                this.setEnabled(true);
                break;
            default:this.setText("Error");;
        }

    }


    @Override
    public void actionPerformed(ActionEvent actionEvent) {



        switch (this.playerState)
        {
            case None:
                this.listener.participantInvited(this.participant);
                break;
            case Invited:
                //Cannot be clicked
                //User have to wait for the response from the Network
                break;
            case ReadyToStart:
                this.listener.invitationAccepted(this.participant);
                break;
            default:;
        }

    }
}
