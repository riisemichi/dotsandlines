package UI;

import UI.Window.EndView.EndView;
import UI.Window.GameView.GameView;
import UI.Window.InvitationView.InvitationView;

/**
 * Factory for Views.
 * Created by michi on 11.05.15.
 */
public class GameViewFactory {
    private static GameViewFactory instance;
    private IGameView gameView;
    private IInvitationView invitationView;
    private IEndView endView;

    public static GameViewFactory getInstance() {
        if(instance == null)
        {
            instance = new GameViewFactory();
        }
        return instance;
    }
    public IGameView getGameView(int width , int height)
    {
        return new GameView(width,height);
    }

    public IInvitationView getInvitationView()
    {
        if(this.invitationView == null)
        {
            //todo: create invitation view instance.
            this.invitationView = new InvitationView();
        }
        return this.invitationView;
    }

    public IEndView getEndView()
    {
        if(this.endView == null)
        {
            //todo: create invitation view instance.
            this.endView = new EndView();
        }
        return this.endView;
    }

    public void resetInstances()
    {
        this.endView = null;
        this.invitationView = null;
        this.gameView = null;
    }
}
