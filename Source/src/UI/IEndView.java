package UI;

import Logic.IPlayer;
import UI.Events.IEndViewListener;
import UI.Events.IMenuListener;

/**
 * Created by deke on 18.05.2015.
 */
public interface IEndView {
    void showView();

    /**
     * Stop displaying the Game View.
     */
    void hideView();

    void setWinner(IPlayer winner);

    /**
     * Register a listener to recieve Event notifications.
     * @param listener the listener that wants to recieve event notifications.
     */
    void registerEndViewEvents(IEndViewListener listener);

    void registerMenuEvents(IMenuListener listener);
}
