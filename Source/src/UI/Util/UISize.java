package UI.Util;

import java.awt.*;

/**
 * Created by deke on 01.05.2015.
 */
public class UISize {

    public static int screenWidth;
    public static int screenHeight;

    public static int xWidth;
    public static int yWidth;

    public static int xOffset;
    public static int yOffset;

    /*
    *Set the Dimension for the JFrame with the maximum possible Height and Width without TaskBar and MenuBar
     */
    static {
       /* Rectangle r = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
        UISize.screenWidth  = (int) r.getWidth();
        UISize.screenHeight = (int) r.getHeight();*/

        UISize.screenWidth  =   800;
        UISize.screenHeight  =   600;

    }

    /*
    *Calculates the absolute width for the Grid, because not every Component have the same width
    * @width Relative width for the Grid. How many fields are in one row
     */
    public static void setXWidth(int width) {
        UISize.xWidth = (int) ((UISize.screenWidth) / (width * 4 + 1));

        //Difference from casting Double to Integer
        UISize.xOffset = (int)((UISize.screenWidth) - UISize.xWidth*(width * 4 + 1))/2;
    }

    /*
    *Calculates the absolute height for the Grid, because not every Component have the same height
    * @height Relative height for the Grid. How many fields are in one column
     */
    public static void setYWidth(int height) {
        UISize.yWidth = (screenHeight / (height * 4 + 1));

        //Difference from casting Double to Integer
        UISize.yOffset = (screenHeight - UISize.yWidth*(height * 4 + 1)) /2;
    }

}
