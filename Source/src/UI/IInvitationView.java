package UI;

import Controller.Controller;
import NetworkManagement.INetworkParticipant;
import UI.Events.IInvitationViewListener;
import UI.Events.IMenuListener;

/**
 * @author Michael Zurmühle
 */
public interface IInvitationView {
    /**
     * Display the InvitationView.
     */
    void showView();

    /**
     * Stop displaying the InvitationView.
     */
    void hideView();

    /**
     * Register a listener to recieve Event notifications.
     * @param listener the listener that wants to recieve event notifications.
     */
    void registerInvitationViewEvents(IInvitationViewListener listener);

    /**
     * Update the list of network Participants.
     * @param participants to display.
     */
    void setParticipantList(INetworkParticipant[] participants);

    void registerMenuEvents(IMenuListener listener);
}
