package UI;

import Logic.IBox;
import Logic.ILocalPlayer;
import Logic.IPlayer;
import Logic.IPlayerMove;
import UI.Events.IEndViewListener;
import UI.Events.IMenuListener;

/**
 * @author Michael Zurmühle
 */
public interface IGameView {
    /**
     * Display the Game View.
     */
    void showView();

    /**
     * Stop displaying the Game View.
     */
    void hideView();

    /**
     * Sets the players for displaying.
     * @param players
     */
    void gameStarted(IPlayer[] players);

    /**
     * Sets the player that is controlled by the UI.
     * @param localPlayer
     */
    void setUIPlayer(ILocalPlayer localPlayer);

    /**
     * Set the list of Moves to display on the UI.
     * @param moves
     */
    void setMoves(IPlayerMove[] moves);

    /**
     * Set the list of boxes to display in the UI.
     * @param boxes
     */
    void setBoxes(IBox[] boxes);

    /**
     * Set the points of a player to display in the UI.
     * @param player
     * @param pointsOfPlayer
     */
    void setPointsOfPlayer(IPlayer player, int pointsOfPlayer);

    void registerMenuEvents(IMenuListener listener);
}
