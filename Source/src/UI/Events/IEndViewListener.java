package UI.Events;

/**
 * @author Michael Zurmühle
 */
public interface IEndViewListener {
    void okSelected();
    void quitGameSelected();
}
