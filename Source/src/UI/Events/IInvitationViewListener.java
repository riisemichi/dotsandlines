package UI.Events;

import NetworkManagement.INetworkParticipant;

/**
 * @author Michael Zurmühle
 */
public interface IInvitationViewListener {
    /**
     * The user selected to start a local game.
     */
    void startLocalGameSelected();

    void startAIGameSelected();

    /**
     * The user invited a participant.
     */
    void participantInvited(INetworkParticipant participant);

    /**
     * The user accepted an invitation of a participant.
     */
    void invitationAccepted(INetworkParticipant participant);

    /**
     * The user changed the name.
     * @param name of the user.
     */
    void nameChanged(String name);
}
