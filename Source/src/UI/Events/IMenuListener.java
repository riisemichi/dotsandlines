package UI.Events;

import Controller.BoardSizes;

import java.io.File;

/**
 * Created by deke on 19.05.2015.
 */
public interface IMenuListener {

    void saveClicked(File file);
    void loadClicked(File file);
    void closeClicked();

    boolean canSave();
    boolean canLoad();
    boolean canSetBoardSize();


    void boardSizeSelected(BoardSizes size);

}
