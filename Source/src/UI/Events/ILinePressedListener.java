package UI.Events;

import java.awt.*;

/**
 * Created by deke on 18.05.2015.
 */
public interface ILinePressedListener {
    void linePressed(Point p1, Point p2);
}
