package AI;

import java.awt.Color;
import java.awt.Point;
import java.util.Date;

import Logic.GameLogicFactory;
import Logic.IGameLogic;
import Logic.ILocalPlayer;
import Logic.IMove;
import Logic.IPlayer;
import Logic.IPlayerMove;

/**
 * Artificial Intelligence for Dots and Lines Game.
 * 
 * @author Tamara Toma
 */

public class GameAI implements IGameAI {

	private ILocalPlayer localPlayer;
	private IGameLogic logic;
	private IMove move;
	private Date time;
	private boolean hasMove = false;
	private int position = 0;

	/**
	 * GameAI constructor.
	 * 
	 * @param logic
	 */
	public GameAI(IGameLogic logic) {
		this.logic = logic;
	}

	/**
	 * This method does the next move in the game.
	 */
	private void nextMove() {

		checkSetTime();
		setPositionForNextMove();

		if (this.logic.isMoveValid(insertLineVertical(position))) {
			movePlayer();
		} else if (this.logic.isMoveValid(insertLineHorizontal(position))) {
			movePlayer();
		} else if (this.logic.isMoveValid(insertLineVerticalLeft(position))) {
			movePlayer();
		} else if (this.logic.isMoveValid(insertLineTop(position))) {
			movePlayer();
		} else {
			if (!checkLineHorizontal()) {
				checkLineVertical();
			}
		}

	}

	/**
	 * This method checks the time. If the variable time is null, the computer
	 * will make the first move in the game.
	 */
	private void checkSetTime() {
		if (this.logic.getBoard().getPlayerMoves().length == 0) {
			doFirstMove();
		} else {
			this.time = this.logic.getBoard().getPlayerMoves()[0].getTime();
		}
	}

	/**
	 * This method sets the next position for the computer.
	 */
	private void setPositionForNextMove() {
		for (int j = 0; j < this.logic.getBoard().getPlayerMoves().length; j++) {
			if (this.logic.getBoard().getPlayerMoves()[j].getTime().compareTo(time) > 0) {
				this.time = this.logic.getBoard().getPlayerMoves()[j].getTime();
				this.position = j;
			}
		}
	}

	/**
	 * This method does the first move on the board.
	 */
	private void doFirstMove() {
		this.move = GameLogicFactory.getInstance().createMove(new Point(0, 0), new Point(0, 1));
		if (this.logic.isMoveValid(this.move)) {
			localPlayer.move(this.move);
		}
	}

	/**
	 * This method inserts a vertical line.
	 * 
	 * @param position
	 * @return IMove
	 */
	private IMove insertLineVertical(int position) {
		if (!(((int) this.logic.getBoard().getPlayerMoves()[position].getMove().getP2().getY() + 1) > this.logic.getBoard().getHeight())) {
			return this.move = GameLogicFactory.getInstance().createMove(
					new Point((int) this.logic.getBoard().getPlayerMoves()[position].getMove().getP2().getX(), (int) this.logic.getBoard().getPlayerMoves()[position].getMove()
							.getP2().getY()),
					new Point((int) this.logic.getBoard().getPlayerMoves()[position].getMove().getP2().getX(), (int) this.logic.getBoard().getPlayerMoves()[position].getMove()
							.getP2().getY() + 1));
		} else {
			return insertLineVerticalLeft(position);
		}

	}

	/**
	 * This method inserts a vertical line on the left side.
	 * 
	 * @param position
	 * @return IMove
	 */
	private IMove insertLineVerticalLeft(int position) {
		return this.move = GameLogicFactory.getInstance().createMove(
				new Point((int) this.logic.getBoard().getPlayerMoves()[position].getMove().getP2().getX(), (int) this.logic.getBoard().getPlayerMoves()[position].getMove().getP2()
						.getY()),
				new Point((int) this.logic.getBoard().getPlayerMoves()[position].getMove().getP2().getX(), (int) this.logic.getBoard().getPlayerMoves()[position].getMove().getP2()
						.getY() - 1));
	}

	/**
	 * This method inserts a horizontal line.
	 * 
	 * @param position
	 * @return IMove
	 */
	private IMove insertLineHorizontal(int position) {
		if (!(((int) this.logic.getBoard().getPlayerMoves()[position].getMove().getP2().getX() + 1) > this.logic.getBoard().getWidth())
				&& (((int) this.logic.getBoard().getPlayerMoves()[position].getMove().getP2().getY() + 1) > this.logic.getBoard().getHeight())) {
			return this.move = GameLogicFactory.getInstance().createMove(
					new Point((int) this.logic.getBoard().getPlayerMoves()[position].getMove().getP2().getX(), (int) this.logic.getBoard().getPlayerMoves()[position].getMove()
							.getP2().getY() + 1),
					new Point((int) this.logic.getBoard().getPlayerMoves()[position].getMove().getP2().getX() + 1, (int) this.logic.getBoard().getPlayerMoves()[position].getMove()
							.getP2().getY() + 1));
		} else {
			return this.move;
		}
	}

	/**
	 * This method inserts a line on the top.
	 * 
	 * @param position
	 * @return IMove
	 */
	private IMove insertLineTop(int position) {
		if (!(((int) this.logic.getBoard().getPlayerMoves()[position].getMove().getP2().getY() - 1) < this.logic.getBoard().getHeight())) {
			return this.move = GameLogicFactory.getInstance().createMove(
					new Point((int) this.logic.getBoard().getPlayerMoves()[position].getMove().getP1().getX(), (int) this.logic.getBoard().getPlayerMoves()[position].getMove()
							.getP1().getY() - 1),
					new Point((int) this.logic.getBoard().getPlayerMoves()[position].getMove().getP2().getX(), (int) this.logic.getBoard().getPlayerMoves()[position].getMove()
							.getP2().getY() - 1));
		} else {
			return this.move;
		}

	}

	/**
	 * This method checks if there are free horizontal lines on the board.
	 * 
	 * @return boolean
	 */
	private boolean checkLineHorizontal() {
		for (int i = 0; i < this.logic.getBoard().getHeight(); i++) {
			for (int j = 0; j < (this.logic.getBoard().getWidth()); j++) {
				this.move = GameLogicFactory.getInstance().createMove(new Point(j, i), new Point(j + 1, i));
				if (this.logic.isMoveValid(this.move)) {
					movePlayer();
					break;
				} else {
					this.move = GameLogicFactory.getInstance().createMove(new Point(j, i + 1), new Point(j + 1, i + 1));
					if (this.logic.isMoveValid(this.move)) {
						movePlayer();
						break;
					} else {
						this.hasMove = false;
					}
				}
			}
		}
		return this.hasMove;
	}

	/**
	 * This method checks if there are free vertical lines on the board.
	 * 
	 * @return boolean
	 */
	private boolean checkLineVertical() {
		for (int i = 0; i < this.logic.getBoard().getWidth(); i++) {
			for (int j = 0; j < (logic.getBoard().getHeight()); j++) {
				this.move = GameLogicFactory.getInstance().createMove(new Point(i, j), new Point(i, j + 1));
				if (this.logic.isMoveValid(this.move)) {
					movePlayer();
					break;
				} else {
					this.hasMove = false;
				}
			}
		}
		return this.hasMove;
	}

	/**
	 * Inserts a line on the board.
	 */
	private void movePlayer() {
		this.localPlayer.move(this.move);
		this.hasMove = true;
	}

	/**
	 * Sets the player.
	 */
	@Override
	public void setPlayer(ILocalPlayer player) {
		this.localPlayer = player;

	}

	/**
	 * Get-Method for the player.
	 */
	@Override
	public ILocalPlayer getPlayer() {
		return this.localPlayer;
	}

	/**
	 * This method does the next move on the board.
	 */
	@Override
	public void doNextMove() {
		nextMove();
	}
}
