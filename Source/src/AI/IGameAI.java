package AI;

import Logic.IBox;
import Logic.ILocalPlayer;
import Logic.IPlayerMove;

/**
 * @author Tamara Toma
 */

public interface IGameAI {

	/**
	 * Set the player which is used by the AI to make a move.
	 * @param player the player.
	 */
	void setPlayer(ILocalPlayer player);

	/**
	 * Get the player which is used by the AI.
	 * @return the player.
	 */
	ILocalPlayer getPlayer();

	/**
	 * Do the next move.
	 */
	void doNextMove();
	
}
