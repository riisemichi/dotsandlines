package Controller;

/**
 * Created by michi on 19.05.15.
 */
public enum BoardSizes {
    Size_1x1 {
        @Override
        public int getHeight()
        {
            return 1;
        }
        @Override
        public int getWidth()
        {
            return 1;
        }
        @Override
        public String toString() {
            return "1x1";
        }
    },
    Size_3x3 {
        @Override
        public int getHeight()
        {
            return 3;
        }
        @Override
        public int getWidth()
        {
            return 3;
        }
        @Override
        public String toString() {
            return "3x3";
        }
    },
    Size_5x5 {
        @Override
        public int getHeight()
        {
            return 5;
        }
        @Override
        public int getWidth()
        {
            return 5;
        }
        @Override
        public String toString() {
            return "5x5";
        }
    },
    Size_10x10{
        @Override
        public int getHeight()
        {
            return 10;
        }
        @Override
        public int getWidth()
        {
            return 10;
        }
        @Override
        public String toString() {
            return "10x10";
        }
    },
    Size_15x15{
        @Override
        public int getHeight()
        {
            return 15;
        }
        @Override
        public int getWidth()
        {
            return 15;
        }
        @Override
        public String toString() {
            return "15x15";
        }
    },;

    public int getHeight() {
        return 0;
    }

    public int getWidth() {
        return 0;
    }
}
