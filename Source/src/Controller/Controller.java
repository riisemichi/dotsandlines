package Controller;

import AI.GameAI;
import AI.IGameAI;
import Logic.*;
import Logic.Events.IGameLogicListener;
import NetworkManagement.*;
import NetworkManagement.Events.IGameNetworkListener;
import Store.BoardData;
import Store.Storage;
import UI.Events.IEndViewListener;
import UI.Events.IInvitationViewListener;
import UI.Events.IMenuListener;
import UI.GameViewFactory;
import UI.IEndView;
import UI.IGameView;
import UI.IInvitationView;

import java.awt.*;
import java.io.File;
import java.util.Arrays;
import java.util.Comparator;


/**
 * @author Michael Zurmühle
 * Created by michi on 11.05.15.
 */
public class Controller implements IGameLogicListener, IGameNetworkListener, IEndViewListener, IInvitationViewListener ,IMenuListener{

    private static final Color Player1Color = Color.BLUE;
    private static final Color Player2Color = Color.RED;

    private static final String AIPlayerName = "Fritz";
    private IGameAI ai;

    private IGameLogic gameLogic;
    private IGameNetworkManager networkManager;
    private INetworkPlayer networkPlayer;

    private IInvitationView invitationView;
    private IGameView gameView;
    private IEndView endView;

    private String name;

    public Controller() {
        // set up Game Logic
        GameLogicFactory gameLogicFactory = GameLogicFactory.getInstance();
        this.gameLogic = gameLogicFactory.getGameLogic();
        this.gameLogic.registerGameLogicEvents(this);

        // set up Network Manager
        NetworkManagerFactory nmf = NetworkManagerFactory.getInstance();
        this.networkManager = nmf.getNetworkManager();
        this.networkManager.registerGameNetworkEvents(this);

        this.initGame();
    }

    private void initGame()
    {
        // Invitation View
        GameViewFactory gvf = GameViewFactory.getInstance();
        gvf.resetInstances();

        this.invitationView = gvf.getInvitationView();
        this.invitationView.registerInvitationViewEvents(this);
        this.invitationView.registerMenuEvents(this);

        // Game View

        // set up EndView
        this.endView = gvf.getEndView();
        this.endView.registerEndViewEvents(this);
        this.endView.registerMenuEvents(this);
        //

        this.invitationView.showView();
        this.networkManager.setName("unknown");
        this.networkManager.startListening();

        // Start Up
        this.setBoardSize(BoardSizes.Size_3x3);
    }

    private void setBoardSize(BoardSizes size) {
        this.gameLogic.setBoardSize(size.getWidth(), size.getHeight());
        this.networkManager.setBoardSize(size.getWidth(), size.getHeight());

        this.gameView = GameViewFactory.getInstance().getGameView(size.getWidth(), size.getHeight());
        this.gameView.registerMenuEvents(this);
    }

    void saveGame(String path)
    {
        if(!path.endsWith(".dnl"))
        {
            path = path + ".dnl";
        }
        Storage store = new Storage();
        store.storeBoard(this.gameLogic.getBoard(), path);
    }

    void loadGame(String path)
    {
        Storage store = new Storage();
        BoardData boardData = store.loadBoard(path);

        IPlayer player1Data = boardData.getPlayerMoves()[0].getPlayer();
        IPlayer player2Data = boardData.getPlayerMoves()[1].getPlayer();

        ILocalPlayer player1 = this.gameLogic.getNewLocalPlayer(player1Data.getName(), player1Data.getColor());
        ILocalPlayer player2 = this.gameLogic.getNewLocalPlayer(player2Data.getName(), player2Data.getColor());
        this.gameLogic.setBoardSize(boardData.getWidth(), boardData.getHeight());
        this.gameLogic.startGame();

        GameLogicFactory factory = GameLogicFactory.getInstance();


        IPlayerMove[] playerMoves = boardData.getPlayerMoves();
        Arrays.sort(playerMoves, (o1, o2) -> o1.getTime().compareTo(o2.getTime()));

        for(IPlayerMove moveData : playerMoves)
        {
            ILocalPlayer player = moveData.getPlayer().equals(player1Data) ? player1 : player2;
            player.move(factory.createMove(moveData.getMove().getP1(), moveData.getMove().getP2()));
        }

        this.invitationView.hideView();
        this.endView.hideView();

        this.gameView.setUIPlayer(player1);
        this.gameView.setUIPlayer(player2);
        this.gameView.showView();
        this.boardChanged();
    }

    /**
     * Prepare and start a network game with a participant.
     * @param participant with which the game should be started.
     */
    private void startNetworkGame(INetworkParticipant participant)
    {
        // prepare and start network game
        ILocalPlayer player = this.gameLogic.getNewLocalPlayer(this.name, Player1Color);
        INetworkPlayer networkPlayer = this.gameLogic.getNewNetworkPlayer(participant.getName(), Player2Color);

        this.networkManager.setBoardSize(this.gameLogic.getBoard().getHeight(), this.gameLogic.getBoard().getWidth());
        this.networkManager.startGame(participant, networkPlayer);
        this.networkPlayer = networkPlayer;

        this.gameLogic.startGame();

        this.gameView.setUIPlayer(player);
    }

    /**
     * Starts a new local game.
     * This starts a game with one Human player and one AI Player.
     */
    private void startAIGame()
    {
        this.ai = new GameAI(this.gameLogic);

        ILocalPlayer player1 = this.gameLogic.getNewLocalPlayer("Player", Player1Color);
        ILocalPlayer player2 = this.gameLogic.getNewLocalPlayer(AIPlayerName, Player2Color);

        this.gameLogic.startGame();
        this.gameView.setUIPlayer(player1);
        this.ai.setPlayer(player2);
    }

    /**
     * Starts a new local game with two human players.
     */
    private void startLocalGame()
    {
        ILocalPlayer player1 = this.gameLogic.getNewLocalPlayer("Player 1", Player1Color);
        ILocalPlayer player2 = this.gameLogic.getNewLocalPlayer("Player 2", Player2Color);

        this.gameLogic.startGame();
        this.gameView.setUIPlayer(player1);
        this.gameView.setUIPlayer(player2);
    }

/************ Invitation View Listener ***************/
    /**
     * The user selected to start a local game.
     */
    @Override
    public void startLocalGameSelected() {
        this.startLocalGame();
    }

    public void startAIGameSelected(){
        this.startAIGame();
    }

    /**
     * The user invited a participant.
     *
     * @param participant that shall be invited
     */
    @Override
    public void participantInvited(INetworkParticipant participant) {
        this.networkManager.inviteParticipant(participant);
        //this.invitationView.setParticipantList(new INetworkParticipant[]{
        //        new NetworkParticipantMock(participant.getName(), participant.getIpAddress(), InvitationState.Invited)});
    }

    /**
     * The user accepted an invitation of a participant.
     *
     * @param participant whose invitation shall be accepted.
     */
    @Override
    public void invitationAccepted(INetworkParticipant participant) {
        this.startNetworkGame(participant);
        //if(participant.getName().equals(AIPlayerName)) {
        //    this.startAIGame();
        //}
    }

    /**
     * The user changed the name.
     * @param name of the user.
     */
    @Override
    public void nameChanged(String name) {
        this.name = name;
        this.networkManager.setName(name);
    }


/************ Game Logic Listener ***************/
    @Override
    public void gameStateChanged() {
        GameState state = this.gameLogic.getState();
        switch (state) {
            case NotReady:
                this.networkManager.startListening();
                this.endView.hideView();
                this.gameView.hideView();
                this.invitationView.hideView();
                this.initGame();
            case Ready:
                break;
            case Started:
                this.networkManager.stopListening();
                this.invitationView.hideView();
                IPlayer[] players = this.gameLogic.getPlayers();
                this.gameView.gameStarted(players);
                this.gameView.showView();
                break;
            case Cancelled:
                break;
            case Ended:
                //Should be replaced by the method from EndView
                this.gameView.hideView();
                this.endView.showView();
                for(IPlayer player : this.gameLogic.getPlayers())
                {
                    this.gameView.setPointsOfPlayer(player, this.gameLogic.getPointsOfPlayer(player));
                }
                this.endView.setWinner(this.gameLogic.getWinner());
                break;
        }
    }

    @Override
    public void nextPlayerChanged() {
        if(this.ai != null && this.gameLogic.getNextPlayer().equals(this.ai.getPlayer()))
        {
            this.ai.doNextMove();
        }
    }

    @Override
    public void moveDone(IPlayerMove playerMove) {
        if (this.networkManager != null && !playerMove.getPlayer().equals(this.networkPlayer))
        {
            this.networkManager.localPlayerMoved(playerMove.getMove());
        }
    }

    @Override
    public void boardChanged() {
        this.gameView.setMoves(this.gameLogic.getBoard().getPlayerMoves());
        this.gameView.setBoxes(this.gameLogic.getBoard().getBoxes());
        for(IPlayer player : this.gameLogic.getPlayers())
        {
            this.gameView.setPointsOfPlayer(player, this.gameLogic.getPointsOfPlayer(player));
        }
    }

/************ Network Manager Listener ***************/
    @Override
    public void participantsListChanged() {
        this.invitationView.setParticipantList(this.networkManager.getAvailableParticipants());
    }

    @Override
    public void participantAcceptedInvitation(INetworkParticipant participant)
    {
        this.startNetworkGame(participant);
//        this.gameLogic.setBoardSize(this.networkManager.boardSizeWidth(participant), this.networkManager.boardSizeHeight(participant));
    }

/************ End View Listener ***************/

    @Override
    public void okSelected() {
        this.gameLogic.reset();
    }

    @Override
    public void quitGameSelected() {
        this.invitationView.hideView();
        this.gameView.hideView();
        this.endView.hideView();
        this.invitationView = null;
        this.gameView = null;
        this.endView = null;

        this.networkManager.stopListening();
        this.networkManager = null;

        System.exit(0);
    }

    /************ Menu Listener ***************/
    @Override
    public void saveClicked(File file) {
        System.out.println(file.getAbsolutePath());
        this.saveGame(file.getAbsolutePath());
        System.out.println("File Saved");
    }

    @Override
    public void loadClicked(File file) {
        System.out.println(file.getAbsolutePath());
        this.loadGame(file.getAbsolutePath());
        System.out.println("File Loaded");
    }

    @Override
    public void closeClicked() {
        System.out.println("Close clicked");
        this.quitGameSelected();
    }

    @Override
    public boolean canSave() {
        return this.gameLogic.getState() == GameState.Started;
    }

    @Override
    public boolean canLoad() {
        return this.gameLogic.getState() == GameState.Ready || this.gameLogic.getState() == GameState.NotReady;
    }

    @Override
    public boolean canSetBoardSize() {
        return this.gameLogic.getState() == GameState.Ready || this.gameLogic.getState() == GameState.NotReady;
    }

    @Override
    public void boardSizeSelected(BoardSizes size) {
        System.out.println("BoardSize selected : " + size.toString());
        this.setBoardSize(size);
    }
}
