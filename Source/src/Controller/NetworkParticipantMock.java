package Controller;

import NetworkManagement.INetworkParticipant;
import NetworkManagement.InvitationState;

/**
 * Created by michi on 19.05.15.
 */
public class NetworkParticipantMock implements INetworkParticipant {
    private String name;
    private String ipadress;

    public NetworkParticipantMock(String name, String ipadress, InvitationState invitationState) {
        this.name = name;
        this.ipadress = ipadress;
        this.invitationState = invitationState;
    }

    private InvitationState invitationState;

    /**
     * @return the name of the participant.
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * @return the IP Adress of the participant.
     */
    @Override
    public String getIpAddress() {
        return this.ipadress;
    }

    /**
     * @return the invitation state of the participant.
     */
    @Override
    public InvitationState getInvitationState() {
        return this.invitationState;
    }
}
