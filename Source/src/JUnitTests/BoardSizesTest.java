package JUnitTests;

import Controller.BoardSizes;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by michi on 19.05.15.
 */
public class BoardSizesTest {
    @Test
    public void testBoardSizeHeight()
    {
        BoardSizes size = BoardSizes.Size_10x10;

        assertEquals(10, size.getHeight());
        assertEquals(10, size.getWidth());

        size = BoardSizes.Size_15x15;
        assertEquals(15, size.getHeight());
        assertEquals(15, size.getWidth());

        size = BoardSizes.Size_5x5;
        assertEquals(5, size.getHeight());
        assertEquals(5, size.getWidth());
    }
}