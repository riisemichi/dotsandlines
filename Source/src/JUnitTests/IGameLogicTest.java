package JUnitTests;

import static org.junit.Assert.*;

import java.awt.Color;
import java.awt.Point;

import Logic.*;
import org.junit.Test;

import Logic.Board;
import Logic.GameLogic;
import Logic.GameLogicFactory;
import Logic.GameState;
import Logic.IGameLogic;
import Logic.ILocalPlayer;

/**
 * @author Tamara Toma
 */

public class IGameLogicTest {
	GameLogicFactory factory = GameLogicFactory.getInstance();
	IGameLogic gameLogic = factory.getGameLogic();
	ILocalPlayer player1 = gameLogic.getNewLocalPlayer("Michi", Color.BLUE);
	ILocalPlayer player2 = gameLogic.getNewLocalPlayer("Sacha", Color.RED);

	@Test
	public void TestNewGame() {
		gameLogic.startGame();
		assertEquals(gameLogic.getState(), GameState.Started);
	}

	@Test
	public void TestAddMove() {
		player1.move(factory.createMove(new Point(5, 9), new Point(6, 9)));
		player2.move(factory.createMove(new Point(5, 10), new Point(6, 10)));
		player1.move(factory.createMove(new Point(5, 11), new Point(6, 9)));
		player2.move(factory.createMove(new Point(1, 2), new Point(2, 2)));
		player1.move(factory.createMove(new Point(1, 1), new Point(2, 1)));
		player2.move(factory.createMove(new Point(1, 3), new Point(2, 3)));
		player1.move(factory.createMove(new Point(1, 4), new Point(2, 4)));
		player2.move(factory.createMove(new Point(1, 5), new Point(2, 5)));

		assertEquals(8, gameLogic.getBoard().getPlayerMoves().length);
	}

	@Test
	public void TestStateTransitionToReady() {
		IGameLogic gameLogic = new GameLogic();
		assertEquals("State should be NotReady in the beginning", GameState.NotReady, gameLogic.getState());

		gameLogic.setBoardSize(10, 10);
		assertEquals(GameState.NotReady, gameLogic.getState());
		ILocalPlayer player1 = gameLogic.getNewLocalPlayer("p1", Color.red);
		assertEquals(GameState.NotReady, gameLogic.getState());
		ILocalPlayer player2 = gameLogic.getNewLocalPlayer("p2", Color.blue);
		assertEquals("State should be ready when everything is defined.", GameState.Ready, gameLogic.getState());

		gameLogic.reset();
		assertEquals("State should be NotReady after", GameState.NotReady, gameLogic.getState());

		player1 = gameLogic.getNewLocalPlayer("p1", Color.red);
		assertEquals(GameState.NotReady, gameLogic.getState());
		player2 = gameLogic.getNewLocalPlayer("p2", Color.blue);
		assertEquals(GameState.NotReady, gameLogic.getState());
		gameLogic.setBoardSize(10, 10);
		assertEquals("State should be ready when everything is defined.", GameState.Ready, gameLogic.getState());

		gameLogic.reset();
		assertEquals("State should be NotReady after", GameState.NotReady, gameLogic.getState());

		player1 = gameLogic.getNewLocalPlayer("p1", Color.red);
		assertEquals(GameState.NotReady, gameLogic.getState());
		gameLogic.setBoardSize(10, 10);
		assertEquals(GameState.NotReady, gameLogic.getState());
		player2 = gameLogic.getNewLocalPlayer("p2", Color.blue);
		assertEquals("State should be ready when everything is defined.", GameState.Ready, gameLogic.getState());

	}

	@Test
	public void TestIsMoveValid() {
		IGameLogic gameLogic = new GameLogic();
		gameLogic.setBoardSize(10, 10);
		ILocalPlayer player1 = gameLogic.getNewLocalPlayer("p1", Color.red);
		gameLogic.startGame();

		IMove invalidDiagonal = factory.createMove(new Point(5, 8), new Point(6, 9));
		assertFalse("Diagonal move should not be valid", gameLogic.isMoveValid(invalidDiagonal));

		IMove invalidOutsideOfBoard1 = factory.createMove(new Point(10, 10), new Point(10, 11));
		IMove invalidOutsideOfBoard2 = factory.createMove(new Point(0, 2), new Point(-1, 2));
		assertFalse("Move outside of the board should not be valid", gameLogic.isMoveValid(invalidOutsideOfBoard1) || gameLogic.isMoveValid(invalidOutsideOfBoard2));

		IMove valid = factory.createMove(new Point(1, 3), new Point(1, 4));
		assertTrue("Normal move should be valid", gameLogic.isMoveValid(valid));

		player1.move(valid);

		assertFalse("Move that was already made should not be valid", gameLogic.isMoveValid(valid));
	}

	@Test
	public void TestBoxesCalculation1() {
		IGameLogic gameLogic = new GameLogic();
		gameLogic.setBoardSize(5, 5);

		ILocalPlayer player1 = gameLogic.getNewLocalPlayer("p1", Color.red);
		ILocalPlayer player2 = gameLogic.getNewLocalPlayer("p2", Color.blue);

		gameLogic.startGame();

		gameLogic.startGame();
		player1.move(factory.createMove(new Point(0, 0), new Point(1, 0)));
		player2.move(factory.createMove(new Point(1, 0), new Point(1, 1)));
		player1.move(factory.createMove(new Point(0, 0), new Point(0, 1)));
		player2.move(factory.createMove(new Point(0, 1), new Point(1, 1)));

		IBox[] boxes = gameLogic.getBoard().getBoxes();
		assertEquals(boxes.length, 1);
		assertEquals("p2", boxes[0].getPlayer().getName());
	}

	@Test
	public void TestBoxesCalculation2() {
		this.gameLogic = new GameLogic();
		this.gameLogic.setBoardSize(5, 5);
		this.player1 = this.gameLogic.getNewLocalPlayer("p1", Color.red);
		this.player2 = this.gameLogic.getNewLocalPlayer("p2", Color.blue);

		this.gameLogic.startGame();
		this.player1.move(factory.createMove(new Point(1, 0), new Point(2, 0)));
		this.player2.move(factory.createMove(new Point(2, 2), new Point(3, 2)));
		this.player1.move(factory.createMove(new Point(2, 0), new Point(2, 1)));
		this.player2.move(factory.createMove(new Point(3, 2), new Point(3, 3)));
		this.player1.move(factory.createMove(new Point(1, 0), new Point(1, 1)));
		this.player2.move(factory.createMove(new Point(2, 2), new Point(2, 3)));
		this.player1.move(factory.createMove(new Point(1, 1), new Point(2, 1))); // box 1x0 closed p1
		this.player1.move(factory.createMove(new Point(2, 1), new Point(2, 2)));
		this.player2.move(factory.createMove(new Point(2, 3), new Point(3, 3))); // box 2x2 closed p2

		IBox[] boxes = this.gameLogic.getBoard().getBoxes();
		assertEquals(boxes.length, 2);
		assertEquals(1, this.gameLogic.getPointsOfPlayer((IPlayer) this.player1));
		assertEquals(1, this.gameLogic.getPointsOfPlayer((IPlayer) this.player2));

		this.player2.move(factory.createMove(new Point(0, 4), new Point(1, 4)));
		this.player1.move(factory.createMove(new Point(1, 4), new Point(1, 5)));
		this.player2.move(factory.createMove(new Point(0, 4), new Point(0, 5)));
		this.player1.move(factory.createMove(new Point(0, 5), new Point(1, 5)));

		assertEquals(2, this.gameLogic.getPointsOfPlayer((IPlayer) this.player1));
		assertEquals(1, this.gameLogic.getPointsOfPlayer((IPlayer) this.player2));
	}

	@Test
	public void TestGameEndedAndWinner() {
		this.gameLogic = new GameLogic();
		this.gameLogic.setBoardSize(3, 2);
		this.player1 = this.gameLogic.getNewLocalPlayer("p1", Color.red);
		this.player2 = this.gameLogic.getNewLocalPlayer("p2", Color.blue);

		this.gameLogic.startGame();

		this.player1.move(factory.createMove(new Point(0, 0), new Point(1, 0)));
		this.player2.move(factory.createMove(new Point(0, 0), new Point(0, 1)));
		this.player1.move(factory.createMove(new Point(1, 0), new Point(1, 1)));

		assertEquals(0, this.gameLogic.getPointsOfPlayer((IPlayer) this.player1));
		assertEquals(0, this.gameLogic.getPointsOfPlayer((IPlayer) this.player2));
		this.player2.move(factory.createMove(new Point(0, 1), new Point(1, 1))); // 4: Field 0x0 closed P2
		assertEquals(0, this.gameLogic.getPointsOfPlayer((IPlayer) this.player1));
		assertEquals(1, this.gameLogic.getPointsOfPlayer((IPlayer) this.player2));

		this.player2.move(factory.createMove(new Point(0, 1), new Point(0, 2)));
		this.player1.move(factory.createMove(new Point(0, 2), new Point(1, 2)));
		this.player2.move(factory.createMove(new Point(1, 1), new Point(1, 2))); // 7: Field 0x1 closed P2
		assertEquals(0, this.gameLogic.getPointsOfPlayer((IPlayer) this.player1));
		assertEquals(2, this.gameLogic.getPointsOfPlayer((IPlayer) this.player2));

		this.player2.move(factory.createMove(new Point(1, 1), new Point(2, 1))); // 8
		this.player1.move(factory.createMove(new Point(2, 1), new Point(3, 1))); // 9
		this.player2.move(factory.createMove(new Point(1, 2), new Point(2, 2))); // 10
		this.player1.move(factory.createMove(new Point(2, 1), new Point(2, 2))); // 11: Field 1x1 closed P1
		assertEquals(1, this.gameLogic.getPointsOfPlayer((IPlayer) this.player1));
		assertEquals(2, this.gameLogic.getPointsOfPlayer((IPlayer) this.player2));

		this.player1.move(factory.createMove(new Point(2, 0), new Point(3, 0))); // 12
		this.player2.move(factory.createMove(new Point(2, 2), new Point(3, 2))); // 13
		this.player1.move(factory.createMove(new Point(3, 1), new Point(3, 2))); // 14: Field 2x1 closed P1
		assertEquals(2, this.gameLogic.getPointsOfPlayer((IPlayer) this.player1));
		assertEquals(2, this.gameLogic.getPointsOfPlayer((IPlayer) this.player2));

		this.player1.move(factory.createMove(new Point(3, 0), new Point(3, 1))); // 15
		this.player2.move(factory.createMove(new Point(2, 0), new Point(2, 1))); // 16: Field 2x0 closed P2
		this.player2.move(factory.createMove(new Point(1, 0), new Point(2, 0))); // 17: Field 1x0 closed P2
		assertEquals(2, this.gameLogic.getPointsOfPlayer((IPlayer) this.player1));
		assertEquals(4, this.gameLogic.getPointsOfPlayer((IPlayer) this.player2));
		// Game ended.

		assertEquals(this.gameLogic.getState(), GameState.Ended);
		IPlayer winner = this.gameLogic.getWinner();
		assertNotNull(winner);
		assertEquals("p2", winner.getName());
	}

	@Test
	public void TestCancelGame() {
		gameLogic.cancelGame();
		assertEquals(gameLogic.getState(), GameState.Cancelled);
	}

	@Test
	public void TestStartGame() {
		gameLogic.startGame();
		assertEquals(gameLogic.getState(), GameState.Started);
	}

	@Test
	public void TestReset() {
		gameLogic.reset();
		assertEquals(gameLogic.getState(), GameState.NotReady);
	}

	@Test
	public void TestSetBoardSize() {

		gameLogic.cancelGame();
		System.out.println(gameLogic.getState());
		gameLogic.setBoardSize(12, 12);
		assertEquals(12, gameLogic.getBoard().getHeight());
		assertEquals(12, gameLogic.getBoard().getWidth());

		gameLogic.startGame();
		gameLogic.setBoardSize(10, 10);
		assertEquals(12, gameLogic.getBoard().getHeight());
		assertEquals(12, gameLogic.getBoard().getWidth());

	}
}
