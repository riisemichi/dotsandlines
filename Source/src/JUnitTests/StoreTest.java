package JUnitTests;

import Store.Storage;
import Logic.*;
import org.junit.Test;

import java.awt.*;
import java.io.File;

import static org.junit.Assert.*;

/**
 * @Author Michael Zurmühle
 * Created by michi on 19.05.15.
 */
public class StoreTest {

    @Test
    public void testSave(){
        GameLogicFactory factory = GameLogicFactory.getInstance();
        IGameLogic gameLogic = factory.getGameLogic();
        gameLogic.setBoardSize(5, 5);

        ILocalPlayer player1 = gameLogic.getNewLocalPlayer("p1", Color.red);
        ILocalPlayer player2 = gameLogic.getNewLocalPlayer("p2", Color.blue);

        gameLogic.startGame();

        player1.move(factory.createMove(new Point(0, 0), new Point(1, 0)));
        player2.move(factory.createMove(new Point(1, 0), new Point(1, 1)));
        player1.move(factory.createMove(new Point(0, 0), new Point(0, 1)));
        player2.move(factory.createMove(new Point(0, 1), new Point(1, 1)));


        String property = "java.io.tmpdir";
        String tempDir = System.getProperty(property);

        String fileName = tempDir+"test.dnl";
        System.out.println(fileName);

        Storage storage = new Storage();
        storage.storeBoard(gameLogic.getBoard(), fileName);


        // Assert
        File f = new File(fileName);
        assertTrue("file should exist", f.exists());
    }

    @Test
    public void testLoad()
    {
        GameLogicFactory factory = GameLogicFactory.getInstance();
        IGameLogic gameLogic = factory.getGameLogic();
        gameLogic.setBoardSize(5, 5);

        ILocalPlayer player1 = gameLogic.getNewLocalPlayer("p1", Color.red);
        ILocalPlayer player2 = gameLogic.getNewLocalPlayer("p2", Color.blue);

        gameLogic.startGame();

        player1.move(factory.createMove(new Point(0, 0), new Point(1, 0)));
        player2.move(factory.createMove(new Point(1, 0), new Point(1, 1)));
        player1.move(factory.createMove(new Point(0, 0), new Point(0, 1)));
        player2.move(factory.createMove(new Point(0, 1), new Point(1, 1)));


        String property = "java.io.tmpdir";
        String tempDir = System.getProperty(property);

        String fileName = tempDir+"test.dnl";
        System.out.println(fileName);

        Storage storage = new Storage();
        storage.storeBoard(gameLogic.getBoard(), fileName);

        // Load
        gameLogic.reset();
        assertEquals(0, gameLogic.getBoard().getPlayerMoves().length);

        IBoard board = storage.loadBoard(fileName);
        assertEquals(4, board.getPlayerMoves().length);
        assertEquals(5, board.getHeight());
        assertEquals(5, board.getWidth());
    }

}