package JUnitTests;

import AI.GameAI;
import AI.IGameAI;
import Logic.*;
import junit.framework.TestCase;
import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by michi on 19.05.15.
 */
public class GameAITest {

    @Test
    public void testFailingCase()
    {
        GameLogicFactory factory = GameLogicFactory.getInstance();
        IGameLogic gameLogic = factory.getGameLogic();
        gameLogic.setBoardSize(1, 1);

        ILocalPlayer player1 = gameLogic.getNewLocalPlayer("p1", Color.red);
        ILocalPlayer aiPlayer = gameLogic.getNewLocalPlayer("p2", Color.blue);

        IGameAI ai = new GameAI(gameLogic);
        ai.setPlayer(aiPlayer);

        //00 10
        player1.move(factory.createMove(new Point(0,0), new Point(1,0)));

        ai.doNextMove();
        assertEquals(2, gameLogic.getBoard().getPlayerMoves().length);

        //00 01
        player1.move(factory.createMove(new Point(0, 0), new Point(1, 0)));
        assertEquals(3, gameLogic.getBoard().getPlayerMoves().length);


        ai.doNextMove();
        assertEquals(4, gameLogic.getBoard().getPlayerMoves().length);

        assertTrue(gameLogic.getWinner().equals(aiPlayer));

    }

}