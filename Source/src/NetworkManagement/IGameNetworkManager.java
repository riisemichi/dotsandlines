package NetworkManagement;

import Logic.IMove;
import Logic.INetworkPlayer;
import NetworkManagement.Events.IGameNetworkListener;

/**
 * The Game Network Manager Interface.
 * @author Michael
 */
public interface IGameNetworkManager {
    /**
     * Tell the Network Manager to start listening to Broarcasts from other participants.
     */
    void startListening();

    /**
     * The name of the local player changed, set it for the next Broadcasts.
     * @param name
     */
    void setName(String name);

    /**
     * Get a list of all the available network participants.
     * @return Array of Network Participants.
     */
    INetworkParticipant[] getAvailableParticipants();

    /**
     * Send an Invite to a specific Network Participant.
     * @param participant to invite.
     */
    void inviteParticipant(INetworkParticipant participant);

    /**
     * Start the game and build up a connection to the participant.
     * @param participant to build up a connection to.
     * @param player as local represenation of the Network Participant.
     */
    void startGame(INetworkParticipant participant, INetworkPlayer player);

    /**
     * Tell the Network Participant, that the local player moved.
     * @param move which the local Player made.
     */
    void localPlayerMoved(IMove move);

    /**
     * Register for the event.
     * Event AvailableParticipansChanged informs about new available Participants or changed states of a participant.
     */
    void registerGameNetworkEvents(IGameNetworkListener listener);

    /**
     * Sets the board size.
     * @param height
     * @param width
     */
    void setBoardSize(int height, int width);


    /**
     * Stop listening to and sending Broadcasts.
     */
    void stopListening();

    /*
    getBoarderSize
     */
    int boardSizeHeight(INetworkParticipant participant);
    int boardSizeWidth(INetworkParticipant participant);

}
