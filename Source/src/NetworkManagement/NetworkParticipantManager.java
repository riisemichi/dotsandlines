/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NetworkManagement;

import Logic.IMove;
import Logic.INetworkPlayer;
import NetworkManagement.Events.IGameNetworkListener;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
/**
 *
 * @author Sacha
 */
public class NetworkParticipantManager {
    SortedMap<String , NetworkParticipant> list = new TreeMap<>();
    IGameNetworkListener iGameNetworkListener;

    public NetworkParticipantManager(IGameNetworkListener iGameNetworkListener){
        this.iGameNetworkListener = iGameNetworkListener;
    }

    public void setNetworkPlayer(NetworkParticipant player){
        if(list.containsKey(player.getIpAddress()) == false)
            list.put(player.getIpAddress(), player);
        else {
            NetworkParticipant exist = list.get(player.getIpAddress());
            exist.setLastSignOfLive();

            checkForOld();
            print();
        }

    }

    /*
    All Elements, which are older than the defined(3000ms) value, will be removed
     */
    public void checkForOld(){
        Set keys =  list.entrySet();
        for (Map.Entry<String, NetworkParticipant> entry : list.entrySet()){
            NetworkParticipant next = entry.getValue();
            Date now = new Date();
            if (now.getTime()-3000 > next.getLastSignOfLive().getTime() && next.getInvitationState().equals(InvitationState.None)){
                iGameNetworkListener.participantsListChanged();
                System.out.println(now.getTime());
                System.out.println(next.getLastSignOfLive().getTime());
                list.remove(entry.getKey());
                break;
            }
        }
    }

    // The Participant are in the Networkmanager dynamic, (.clone)
    public INetworkParticipant[] getAvailableParticipants() {
        List<NetworkParticipant> array = new ArrayList<>();
        for(Map.Entry<String, NetworkParticipant> entry : list.entrySet()){
            if (entry.getValue().getIpAddress().equals(getOwnIPAddress())) {continue;} //remove yourself
            array.add(entry.getValue()); //(.clone)
        }
        return array.toArray(new NetworkParticipant[array.size()]);
    }

    public INetworkParticipant getAvailableParticipant(String ipAddress){
        return (INetworkParticipant) list.get(ipAddress);
    }

    public INetworkParticipant getAvailableParticipantByName(String name){
        Set keys = list.entrySet();
        for(Map.Entry<String, NetworkParticipant> entry : list.entrySet()){
            NetworkParticipant next = entry.getValue();
            if (next.getName().equals(name)){
                iGameNetworkListener.participantsListChanged();
                return next;
            }
        }
        return null;
    }

    public void print(){
        Set keys =  list.entrySet();
        System.out.print("Players are added " + new Date() + "------");
        for (Map.Entry<String, NetworkParticipant> entry : list.entrySet()){
            NetworkParticipant next = entry.getValue();
            System.out.print("," + next.getIpAddress());
            System.out.print("," + next.getName());
            System.out.print("," + next.getInvitationState());
        }
        System.out.println("");
    }

    public void setNetworkParticipant(NetworkParticipant networkParticipantMyself, String packetIPSend) {
        if (    networkParticipantMyself.getIpAddress()       != null &&
                networkParticipantMyself.getName()            != null &&
                networkParticipantMyself.getInvitationState() != null &&
                networkParticipantMyself.getName().length()   >  0) {

            /*
            Lookup Participant, if already contains in the list
            */
            Boolean containIpAddress;
            containIpAddress = list.containsKey(packetIPSend);

            /*
            it is used for Broadcast listening/sniffing
             */
            if (networkParticipantMyself.getInvitationState().equals(InvitationState.None)) {
                if (!containIpAddress) {
                    NetworkParticipant np = new NetworkParticipant();
                    np.setName(networkParticipantMyself.getName());
                    np.setIpAddress(networkParticipantMyself.getIpAddress());
                    np.setInvitationState(InvitationState.None);
                    list.put(networkParticipantMyself.getIpAddress(), np);
                } else {
                    NetworkParticipant networkParticipant = list.get(packetIPSend);
                    if (containIpAddress && !networkParticipant.getName().equals(networkParticipantMyself.getName()) ) {
                        networkParticipant.setName(networkParticipantMyself.getName());
                    }
                }
                iGameNetworkListener.participantsListChanged();
            }
            /*
            if someone invites somebody, it changes the invited.
             */
            if (networkParticipantMyself.getInvitationState().equals(InvitationState.Invited) && !containIpAddress) {

                NetworkParticipant networkParticipant = list.get(packetIPSend);
                networkParticipant.setInvitationState(InvitationState.ReadyToStart);
                iGameNetworkListener.participantsListChanged();
            }
            if (networkParticipantMyself.getInvitationState().equals(InvitationState.ReadyToStart) && networkParticipantMyself.getiMove() == null) {
                NetworkParticipant networkParticipant = list.get(packetIPSend);
                networkParticipant.setInvitationState(InvitationState.ReadyToStart);
                iGameNetworkListener.participantsListChanged();
                iGameNetworkListener.participantAcceptedInvitation(networkParticipant);
            }
            if (networkParticipantMyself.getInvitationState().equals(InvitationState.ReadyToStart) && networkParticipantMyself.getiMove() != null) {
                list.get(getOwnIPAddress()).getNetworkPlayer().move(networkParticipantMyself.getiMove());
            }
        }
    }

    /*
    set Network player for move responses.
     */
    public void startGame(INetworkParticipant participant, INetworkPlayer player) {
        NetworkParticipant participantInList = list.get(participant.getIpAddress());
        participantInList.setNetworkPlayer(player);
    }

    public NetworkParticipant getNetworkParticipantWithPlayer() {
        Set keys = list.entrySet();
        for (Map.Entry<String, NetworkParticipant> entry : list.entrySet()) {
            NetworkParticipant next = entry.getValue();
            if (next.getNetworkPlayer() != null || next.getInvitationState().equals(InvitationState.ReadyToStart))
                return next;
        }
        return null;
    }

    public void inviteParticipant(NetworkParticipant participant) {
        list.get(participant.getIpAddress()).setInvitationState(InvitationState.Invited);
        iGameNetworkListener.participantsListChanged();
    }

    public String getOwnIPAddress(){
        try {
            return Inet4Address.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return "";
    }

    public int boardSizeWidth(INetworkParticipant participant) {
        return list.get(participant).getSizeWidth();
    }

    public int boardSizeHeight(INetworkParticipant participant) {
        return list.get(participant).getSizeWidth();
    }
}