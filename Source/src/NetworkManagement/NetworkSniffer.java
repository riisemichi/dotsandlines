/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NetworkManagement;

import java.io.*;
import java.net.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 *
 * @author Sacha
 */
public class NetworkSniffer implements Runnable {
    private final int port;

    /*
    @param networkParticipantManager there would be saved all participant, which are found in the network
    @param eventListener
     */
    NetworkParticipantManager networkParticipantManager;
    private boolean sniffer;

    public NetworkSniffer(NetworkParticipantManager networkParticipantManager, int port){
        this.networkParticipantManager = networkParticipantManager;
        this.port = port;
    }

    @Override
    public void run() {
        sniffer = true;
        while (sniffer){
            DatagramPacket packet = new DatagramPacket(new byte[1024], 1024);
            try ( DatagramSocket socket = new DatagramSocket(port)){
                socket.receive(packet);
            } catch (BindException ex) {
                ex.printStackTrace();
                System.out.println("Couldn't start DatagramSocket. It might be already started");
                try {
                    // 
                    TimeUnit.MILLISECONDS.sleep(1000);
                } catch (InterruptedException ex1) {
                    Logger.getLogger(NetworkSniffer.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }catch (IOException ex) {
                Logger.getLogger(NetworkSniffer.class.getName()).log(Level.SEVERE, null, ex);
                break;
            }
                InetAddress adresse = packet.getAddress();
                int port = packet.getPort();
                int len = packet.getLength();

                byte[] data = packet.getData();

                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(data);
            try {
                ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
                NetworkParticipant networkParticipantMyself = (NetworkParticipant) objectInputStream.readObject();

                System.out.println("incoming Packet from: " + NetworkParticipant.shortingIpAdress(packet.getAddress().toString()) + " with the Participant IP-Address: "
                        + networkParticipantMyself.getIpAddress() + " and the InvitationState: " + networkParticipantMyself.getInvitationState());
                if (networkParticipantMyself.getiMove() != null)
                    System.out.print("");
                networkParticipantManager.setNetworkParticipant(networkParticipantMyself, NetworkParticipant.shortingIpAdress(packet.getAddress().toString()));
            } catch (StreamCorruptedException ex){
                // ex.printStackTrace();
            }
            catch (IOException e) {
                //e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }


            if (adresse != null && Pattern.matches("^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}$",adresse.getHostAddress()) && networkParticipantManager != null) {
                NetworkParticipant set = new NetworkParticipant();
                set.setIpAddress(adresse.getHostAddress());
                networkParticipantManager.setNetworkPlayer(set);
            } else if(adresse!=null) {
                System.out.println(Pattern.matches("^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}$",adresse.getHostAddress())+ adresse.getHostAddress());
            }
        }
    }
    public void stopBroadcast() {
        sniffer = false;
    }
}
