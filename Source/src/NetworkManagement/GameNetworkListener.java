package NetworkManagement;

import Logic.IMove;
import NetworkManagement.Events.IGameNetworkListener;

/**
 * Created by User on 16.05.2015.
 */
public class GameNetworkListener implements IGameNetworkListener
{
    @Override
    public void participantsListChanged() {
        System.out.println("It occur a change in the participance list ");
    }

    @Override
    public void participantAcceptedInvitation(INetworkParticipant participant) {
        System.out.println("This player has invited the players: " + participant.getIpAddress());
    }
}