/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NetworkManagement;

import Logic.IMove;
import Logic.INetworkPlayer;

import java.io.Serializable;
import java.util.Date;
/**
 *
 * @author Sacha
 */
public class NetworkParticipant implements INetworkParticipant, Comparable<NetworkParticipant>, Cloneable, Serializable {

    private String ipAddress;
    private String name;
    private InvitationState invitationState;
    private int boarderHeight;
    private int boarderWidth;
    private IMove iMove;
    private transient Date lastSignOfLife;
    private transient INetworkPlayer networkPlayer;
    private transient byte[] ipAddressSend;

    /*
     * Init the class with Date (milliseconds)
     */
    public NetworkParticipant(){
        lastSignOfLife = new Date();
    }

    public Date getLastSignOfLive(){
        return lastSignOfLife;
    }

    public void setLastSignOfLive(){
        lastSignOfLife = new Date();
    }

    @Override
    public String getIpAddress(){
        return ipAddress;
    }

    public void setIpAddress(String ipAddress){
        this.ipAddress = shortingIpAdress(ipAddress);
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public InvitationState getInvitationState() {
        return invitationState;
    }

    public void setInvitationState(InvitationState invitationState) {
        this.invitationState = invitationState;
    }

    public INetworkPlayer getNetworkPlayer() {
        return networkPlayer;
    }
    public int compareTo(NetworkParticipant t) {
        return this.name.compareTo(t.getName());
    }

    public void setNetworkPlayer(INetworkPlayer networkPlayer) {
        this.networkPlayer = networkPlayer;
    }

    /*
     * it's not anymore used. It was supposed for UI but I changed Idea
     */
    public NetworkParticipant clone() {
        NetworkParticipant clone = new NetworkParticipant();
        if (ipAddress != null)
            clone.setIpAddress(new String(ipAddress));
        if (name != null)
            clone.setName(new String(name));
        clone.setInvitationState(invitationState);
        return clone;
    }

    /*
     * The java.io.InetAddress gives the Address like: name/192.168.1.20
     * This class cuts the String at the beginning
     */
    public static String shortingIpAdress(String ipAddress){
        if (ipAddress == null) {
            return null;
        };
        String[] a = ipAddress.split("/");
        if (a.length == 2) {
            return a[1];
        } else {
            return a[0];
        }
    }

    public void setIpAddressSend(byte[] ipAddressSend) {
        this.ipAddressSend = ipAddressSend;
    }

    public byte[] getIpAddressSend() {
        return ipAddressSend;
    }

    public IMove getiMove() {
        return iMove;
    }

    public void setiMove(IMove iMove) {
        this.iMove = iMove;
    }

    public void setboarder(int height, int width){
        this.boarderHeight = height;
        this.boarderWidth = width;
    }

    public int getSizeWidth() {
        return boarderWidth;
    }

    public int getSizeHeight() {
        return boarderHeight;
    }
}
