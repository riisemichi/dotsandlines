/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NetworkManagement;

import Logic.IMove;
import NetworkManagement.Events.IGameNetworkListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
/**
 *
 * @author Sacha
 */
public class NetworkBroadcaster implements Runnable{
    private String name;
    private boolean reciveData;

    /*
     (invitedParticipant or startParticipant) is used for sending invitation.
     */
    private NetworkParticipant invitedParticipant;
    private NetworkParticipant startParticipant;

    private IMove move;
    private NetworkParticipantManager partVer;
    private int port;
    private boolean broadcast;
    private int boardSizeWidth;
    private int boardSizeHeight;

    public NetworkBroadcaster(NetworkParticipantManager networkParticipantManager, int port) {
        this.port = port;
        this.partVer = networkParticipantManager;
    }

    @Override
    public void run() {

                try (DatagramSocket toSocket = new DatagramSocket()){
                broadcast = true;
                while (broadcast) {
                    for(int i = 0; i < 20; i++){ //?? replace with notifyAll()
                        Thread.sleep(50);
                        if (reciveData) {
                            continue;
                        }
                    }
                    NetworkParticipant serializableParticipant = new NetworkParticipant();
                    serializableParticipant.setName(name);

                    if (reciveData) {
                        reciveData = false;
                        /*
                        Build the packet, for invitation
                         */
                        if (invitedParticipant != null) {
                            serializableParticipant.setIpAddress(invitedParticipant.getIpAddress());
                            serializableParticipant.setInvitationState(InvitationState.Invited);

                            InetAddress ip = InetAddress.getByName(invitedParticipant.getIpAddress());
                            serializableParticipant.setIpAddressSend(ip.getAddress());
                            invitedParticipant = null;

                        } else if (startParticipant != null){
                            /*
                             * Build the packet for join a game
                             */
                            serializableParticipant.setIpAddress(startParticipant.getIpAddress());
                            serializableParticipant.setInvitationState(InvitationState.ReadyToStart);

                            InetAddress ip = InetAddress.getByName(startParticipant.getIpAddress());
                            startParticipant = null;
                            serializableParticipant.setIpAddressSend(ip.getAddress());

                        } else if ( move != null) {
                            /*
                            Send movement
                             */
                            NetworkParticipant playingParticipant = partVer.getNetworkParticipantWithPlayer();

                            /*
                            There is no Participant, for sending the Packet
                             */
                            if(playingParticipant != null) {
                                /*
                                 * Sends a move
                                 */
                                serializableParticipant.setIpAddress(playingParticipant.getIpAddress());
                                serializableParticipant.setInvitationState(InvitationState.ReadyToStart);

                                InetAddress ip = InetAddress.getByName(playingParticipant.getIpAddress());
                                serializableParticipant.setIpAddressSend(ip.getAddress());

                                serializableParticipant.setiMove(move);
                            } else{
                                continue;
                            }
                        }
                    } else {
                        /*
                         * sends a Broadcast
                         */
                        serializableParticipant.setIpAddress(NetworkParticipant.shortingIpAdress(InetAddress.getLocalHost().toString()));
                        serializableParticipant.setInvitationState(InvitationState.None);
                        serializableParticipant.setIpAddressSend(NetworkBroadcaster.getBroadcast());
                    }

                    serializableParticipant.setboarder(boardSizeHeight, boardSizeWidth);

                    /*
                    The Packet, would be send here.
                     */
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    ObjectOutputStream os = new ObjectOutputStream(byteArrayOutputStream);
                    os.writeObject(serializableParticipant);

                    byte[] data = byteArrayOutputStream.toByteArray();
                    InetAddress ia = InetAddress.getByAddress(serializableParticipant.getIpAddressSend());
                    System.out.println("sending Packet to: " + ia.getHostAddress() + " with the Participant IP-Address: "
                            + serializableParticipant.getIpAddress() + " and the InvitationState: " + serializableParticipant.getInvitationState()
                            + " " + this);
                    DatagramPacket packet = new DatagramPacket(data , data.length, ia, port );
                    toSocket.send(packet);
                }
            } catch (InterruptedException | IOException ex) {
                ex.printStackTrace();
            }
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static byte[] getBroadcast(){
        return new byte[] {(byte)255,(byte)255,(byte)255,(byte)255};
    }

    public synchronized void inviteParticipant(NetworkParticipant participant) {
        reciveData = true;
        invitedParticipant = participant;
    }

    public synchronized void startGame(INetworkParticipant participant) {
        reciveData = true;
        this.startParticipant = (NetworkParticipant) participant;
    }

    public synchronized void localPlayerMoved(IMove move) {
        reciveData = true;
        this.move = move;
    }

    public void stopBroadcast() {
        broadcast = false;
    }

    public void setBoardSize(int height, int width) {
        this.boardSizeHeight = height;
        this.boardSizeWidth = width;
    }
}
