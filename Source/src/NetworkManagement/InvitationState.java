package NetworkManagement;

public enum InvitationState
{
    // Not invited this participant, and not invited by this participant.
    None,

    // I invited this participant
    Invited,

    invitationState, // This participant invited me.
    ReadyToStart
}
