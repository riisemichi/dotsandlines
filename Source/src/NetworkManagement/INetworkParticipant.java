package NetworkManagement;

import Logic.INetworkPlayer;

/**
 * A Participant that is found on the network.
 * @author Michael
 */
public interface INetworkParticipant {
    /**
     * @return the name of the participant.
     */
    String getName();

    /**
     * @return the IP Adress of the participant.
     */
    String getIpAddress();

    /**
     * @return the invitation state of the participant.
     */
    InvitationState getInvitationState();

}
