package NetworkManagement.Events;

import Logic.IMove;
import Logic.INetworkPlayer;
import Logic.Move;
import NetworkManagement.INetworkParticipant;

/**
 * @author Michael
 */
public interface IGameNetworkListener {
    /**
     * The list of available participants changed.
     */
    void participantsListChanged();

    /**
     * A previously invited participant accepted the invitation.
     */
    void participantAcceptedInvitation(INetworkParticipant participant);

}
