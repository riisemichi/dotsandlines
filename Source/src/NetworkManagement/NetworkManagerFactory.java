package NetworkManagement;

/**
 * Created by Sacha on 11.05.2015.
 */
public class NetworkManagerFactory {
    private static NetworkManagerFactory instance;
    private IGameNetworkManager networkManager;

    public static NetworkManagerFactory getInstance()
    {
        if(instance == null)
        {
            instance = new NetworkManagerFactory();
        }
        return instance;
    }

    public IGameNetworkManager getNetworkManager()
    {
        if(networkManager == null)
        {
            networkManager = new NetworkManager();
        }
        return networkManager;
    }
}
