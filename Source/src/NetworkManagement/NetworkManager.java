package NetworkManagement;

import Logic.IMove;
import Logic.INetworkPlayer;
import NetworkManagement.Events.IGameNetworkListener;

/**
 * Created by Sacha on 11.05.2015.
 */
public class NetworkManager implements IGameNetworkManager{
    NetworkParticipantManager networkParticipantManager;
    private IGameNetworkListener gameNetworkListener;

    NetworkSniffer networkSniffer;
    NetworkBroadcaster networkBroadcaster;

    int port = 1300;
    private String name;

    public NetworkManager(){
    }

    @Override
    public void startListening(){
        networkParticipantManager = new NetworkParticipantManager(gameNetworkListener);
        networkSniffer = new NetworkSniffer(networkParticipantManager, port);
        networkBroadcaster = new NetworkBroadcaster(networkParticipantManager, port);

        networkBroadcaster.setName(name);
        if (networkBroadcaster.getName() == null || networkBroadcaster.getName().length() <= 0) {
            return;
        }
        new Thread(networkBroadcaster).start();
        new Thread(networkSniffer).start();
    }

    @Override
    public void setName(String name) {
        this.name = name;
        if (networkBroadcaster != null) {
            networkBroadcaster.setName(name);
        }
    }

    @Override
    public INetworkParticipant[] getAvailableParticipants() {
        return networkParticipantManager.getAvailableParticipants();
    }

    @Override
    public void inviteParticipant(INetworkParticipant participant) {
        networkBroadcaster.inviteParticipant((NetworkParticipant) participant);
        networkParticipantManager.inviteParticipant((NetworkParticipant) participant);
    }

    @Override
    public void startGame(INetworkParticipant participant, INetworkPlayer player) {
        networkBroadcaster.startGame(participant);
        networkParticipantManager.startGame(participant, player);
    }

    @Override
    public void localPlayerMoved(IMove move) {
        networkBroadcaster.localPlayerMoved(move);
    }

    @Override
    public void registerGameNetworkEvents(IGameNetworkListener listener) {
        this.gameNetworkListener = listener;
    }

    @Override
    public void setBoardSize(int height, int width) {
        networkBroadcaster.setBoardSize(height, width);
    }

    /**
     * Stop listening and sending Broadcasts.
     */
    @Override
    public void stopListening() {
        networkBroadcaster.stopBroadcast();
        networkSniffer.stopBroadcast();
        networkSniffer = null;
        networkBroadcaster = null;
    }

    @Override
    public int boardSizeHeight(INetworkParticipant participant) {
        return networkParticipantManager.boardSizeHeight(participant);
    }

    @Override
    public int boardSizeWidth(INetworkParticipant participant) {
        return networkParticipantManager.boardSizeWidth(participant);
    }
}
