package Store;

import Logic.IBoard;
import Logic.IBox;
import Logic.IPlayerMove;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @Author Michael Zurmühle
 * Created by michi on 19.05.15.
 */
public class BoardData implements IBoard, Serializable {
    private int height;
    private int width;
    private BoxData[] boxes;

    private PlayerMoveData[] playerMoves;

    public void setBoxes(BoxData[] boxes) {
        this.boxes = boxes;
    }

    public void setPlayerMoves(PlayerMoveData[] playerMoves) {
        this.playerMoves = playerMoves;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public IPlayerMove[] getPlayerMoves() {
        return this.playerMoves;
    }

    @Override
    public IBox[] getBoxes() {
        return this.boxes;
    }

    static BoardData fromIBoard(IBoard object)
    {
        BoardData data = new BoardData();
        data.setHeight(object.getHeight());
        data.setWidth(object.getWidth());
        ArrayList<PlayerMoveData> moves = new ArrayList<>();
        for(IPlayerMove playerMove : object.getPlayerMoves())
        {
            moves.add(PlayerMoveData.fromIPlayerMove(playerMove));
        }
        data.setPlayerMoves(moves.toArray(new PlayerMoveData[moves.size()]));
        ArrayList<BoxData> boxes = new ArrayList<>();
        for(IBox box : object.getBoxes())
        {
            boxes.add(BoxData.fromIBox(box));
        }
        data.setBoxes(boxes.toArray(new BoxData[boxes.size()]));

        return data;
    }
}
