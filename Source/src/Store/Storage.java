package Store;

import Logic.IBoard;

import java.io.*;

/**
 * @Author Michael Zurmühle
 * Created by michi on 19.05.15.
 */
public class Storage {
    public void storeBoard(IBoard board, String fileName)
    {
        BoardData data = BoardData.fromIBoard(board);

        try {
            FileOutputStream fout = new FileOutputStream(fileName);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(data);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public BoardData loadBoard(String url)
    {
        BoardData data = null;

        FileInputStream fin = null;
        try {
            fin = new FileInputStream(url);
            ObjectInputStream ois = new ObjectInputStream(fin);
            data = (BoardData) ois.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return data;
    }
}
