package Store;

import Logic.IBox;
import Logic.IPlayer;

import java.awt.*;
import java.io.Serializable;

/**
 * @Author Michael Zurmühle
 * Created by michi on 19.05.15.
 */
public class BoxData implements IBox, Serializable {
    private PlayerData player;
    private Point point;

    public void setPlayer(PlayerData player) {
        this.player = player;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    /**
     * @return The player that owns this box.
     */
    @Override
    public IPlayer getPlayer() {
        return this.player;
    }

    /**
     * @return The position of the box.
     * The position is equal to the point on the top left of the box.
     * <p>
     * 00    01   02
     * +----+----+
     * | 00 | 01 |
     * +----+----+
     * 10    11   12
     */
    @Override
    public Point getPoint() {
        return this.point;
    }


    public static BoxData fromIBox(IBox box) {
        BoxData data = new BoxData();
        data.setPoint(box.getPoint());
        data.setPlayer(PlayerData.fromIPlayer(box.getPlayer()));
        return data;
    }
}
