package Store;

import Logic.IMove;

import java.awt.*;
import java.io.Serializable;

/**
 * @Author Michael Zurmühle
 * Created by michi on 19.05.15.
 */
public class MoveData implements IMove, Serializable {
    private Point p1, p2;

    public void setP1(Point p1) {
        this.p1 = p1;
    }

    public void setP2(Point p2) {
        this.p2 = p2;
    }

    /**
     * @return First Point (x,y).
     */
    @Override
    public Point getP1() {
        return this.p1;
    }

    /**
     * @return Second Point (x,y).
     */
    @Override
    public Point getP2() {
        return this.p2;
    }

    @Override
    public boolean isHorizontalLine() {
        return false;
    }

    public static MoveData fromIMove(IMove objectMove) {
        MoveData data = new MoveData();
        data.setP1(objectMove.getP1());
        data.setP2(objectMove.getP2());
        return data;
    }
}
