package Store;

import Logic.IPlayer;

import java.awt.*;
import java.io.Serializable;
import java.util.HashMap;

/**
 * @Author Michael Zurmühle
 * Created by michi on 19.05.15.
 */
public class PlayerData implements IPlayer, Serializable {
    private Color color;
    private String name;

    public void setColor(Color color) {
        this.color = color;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the color of the player.
     */
    @Override
    public Color getColor() {
        return this.color;
    }

    /**
     * @return the name of the player.
     */
    @Override
    public String getName() {
        return this.name;
    }

    public static PlayerData fromIPlayer(IPlayer player) {
        PlayerData data = new PlayerData();
        data.setColor(player.getColor());
        data.setName(player.getName());
        return data;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
        {
            return false;
        }

        if(obj.getClass() != this.getClass())
        {
            return false;
        }

        PlayerData other = (PlayerData)obj;
        return other.getName() == this.getName() && other.getColor() == this.getColor();
    }
}
