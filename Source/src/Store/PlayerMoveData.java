package Store;

import Logic.IMove;
import Logic.IPlayer;
import Logic.IPlayerMove;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author Michael Zurmühle
 * Created by michi on 19.05.15.
 */
public class PlayerMoveData implements IPlayerMove, Serializable {
    private Date date;
    private PlayerData player;
    private MoveData move;

    public void setTime(Date date) {
        this.date = date;
    }

    public void setPlayer(PlayerData player) {
        this.player = player;
    }

    public void setMove(MoveData move) {
        this.move = move;
    }

    @Override
    public Date getTime() {
        return this.date;
    }

    @Override
    public IPlayer getPlayer() {
        return this.player;
    }

    @Override
    public IMove getMove() {
        return this.move;
    }

    public static PlayerMoveData fromIPlayerMove(IPlayerMove object) {
        PlayerMoveData data = new PlayerMoveData();
        data.setTime(object.getTime());
        data.setPlayer(PlayerData.fromIPlayer(object.getPlayer()));
        data.setMove(MoveData.fromIMove(object.getMove()));
        return data;
    }
}
