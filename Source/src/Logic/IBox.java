package Logic;

import java.awt.*;

/**
 * @author Michael Zurmühle
 */
public interface IBox {
    /**
     * @return The player that owns this box.
     */
    IPlayer getPlayer();

    /**
     * @return The position of the box.
     * The position is equal to the point on the top left of the box.
     *
     * 00    01   02
     *  +----+----+
     *  | 00 | 01 |
     *  +----+----+
     * 10    11   12
     */
    Point getPoint();
}
