package Logic;

import java.util.Date;


/**
 * @author Tamara Toma
 */
public class PlayerMove implements IPlayerMove{
	private Date time;
	private IPlayer player;
	private IMove move;

	public PlayerMove(IPlayer player, IMove move) {
		this.player = player;
		this.move = move;
		this.time= new Date();
	}

	@Override
	public Date getTime() {
		return time;
	}


	@Override
	public IPlayer getPlayer() {
		return player;
	}

	@Override
	public IMove getMove() {
		return this.move;
	}

}
