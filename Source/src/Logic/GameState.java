package Logic;


/**
 * @author Tamara Toma
 */

public enum GameState {
    NotReady,
    Ready,
    Started,
    Cancelled,
    Ended
}
