package Logic;

import java.awt.Point;

/**
 * Created by michi on 11.05.15.
 */
public class GameLogicFactory {
	private static GameLogicFactory instance;
	private static IGameLogic gameLogic;

	public static GameLogicFactory getInstance() {
		if (instance == null) {
			instance = new GameLogicFactory();
		}
		return instance;
	}

	/**
	 * Gets the instance of Game Logic.
	 * 
	 * @return the game logic.
	 */
	public IGameLogic getGameLogic() {
		if (gameLogic == null) {
			gameLogic = new GameLogic();
		}
		return gameLogic;
	}

	public IMove createMove(Point p1, Point p2) {
		IMove move = new Move(p1, p2);
		return move;
	}
	
	
}
