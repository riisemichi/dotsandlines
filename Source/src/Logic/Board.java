package Logic;

import java.awt.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Tamara Toma
 */
public class Board implements IBoard {
	private int width;
	private int height;
	private List<IPlayerMove> moves;
	private List<IBox> boxes;
	
	public Board(){
		this.moves = new LinkedList<>();
		this.boxes = new LinkedList<>();
	}
	
	public Board(int width, int height) {
		this.width = width;
		this.height = height;
		this.moves = new LinkedList<>();
	}


	private boolean checkBoxesForMove(IMove move) {
		boolean boxClosed = false;
		if (move.isHorizontalLine())
		{
			if(move.getP1().y >0) {
				boxClosed |= checkBox(new Point(move.getP1().x, move.getP1().y - 1));
			}
			if(move.getP1().y < this.height) {
				boxClosed |= checkBox(move.getP1());
			}
		}
		else
		{
			if(move.getP1().x > 0) {
				boxClosed |= checkBox(new Point(move.getP1().x - 1, move.getP1().y));
			}
			if(move.getP1().x < this.width) {
				boxClosed |= checkBox(move.getP1());
			}
		}
		return boxClosed;
	}

	/**
	 * Checks for a specific box position, if it belongs to a player
	 * @param point on the top left of the box to check.
	 */
	private boolean checkBox(Point point)
	{
		// define moves that are relevant for the box.
		Move moveTop = new Move(
				point,
				new Point(point.x+1, point.y));
		Move moveLeft = new Move(
				point,
				new Point(point.x, point.y+1));
		Move moveRight = new Move(
				new Point(point.x+1, point.y),
				new Point(point.x+1, point.y+1));
		Move moveBottom = new Move(
				new Point(point.x, point.y+1),
				new Point(point.x+1, point.y+1));

		// see if all relevant moves are already in the field
		ArrayList<IPlayerMove> relevantMoves = new ArrayList<>();
		for(IPlayerMove pMove : this.moves)
		{
			if(pMove.getMove().equals(moveTop) ||
					pMove.getMove().equals(moveLeft) ||
					pMove.getMove().equals(moveRight) ||
					pMove.getMove().equals(moveBottom)  )
			{
				relevantMoves.add(pMove);
			}
		}

		// Set the winner of the box.
		if(relevantMoves.size() >= 4)
		{
			relevantMoves.sort((o1, o2) -> o1.getTime().compareTo(o2.getTime()));
			IPlayerMove newestMove = relevantMoves.get(relevantMoves.size()-1);

			Box box = new Box(newestMove.getPlayer(), point);
			this.boxes.add(box);
			return true;
		}
		return false;
	}


	/**
	 * @param playerMove
	 * @return true: the move closed a box, false: the move didn't close a box.
	 */
	public boolean addPlayerMove(IPlayerMove playerMove){
		this.moves.add(playerMove);
		return this.checkBoxesForMove(playerMove.getMove());
	}

	@Override
	public int getHeight() {
		return this.height;
	}

	@Override
	public int getWidth() {
		return this.width;
	}
	@Override
	public IPlayerMove[] getPlayerMoves() {
		return this.moves.toArray(new IPlayerMove[this.moves.size()]);
	}
	
	@Override
	public IBox[] getBoxes() {
		return this.boxes.toArray(new IBox[this.boxes.size()]);
	}

	protected void setHeight(int height) {
		this.height = height;
	}

	protected void setWidth(int width) {
		this.width = width;
	}
}
