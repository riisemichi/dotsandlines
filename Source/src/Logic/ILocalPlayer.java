package Logic;

import java.awt.Color;


/**
 * @author Tamara Toma
 */

public interface ILocalPlayer {

	/**
	 * @return The color of the player.
	 */
	Color getColor();

	/**
	 * @return The name of the Player.
	 */
	String getName();

	/**
	 * @param move make a move.
	 */
	void move(IMove move);
}
