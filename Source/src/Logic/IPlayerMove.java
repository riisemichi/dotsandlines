package Logic;

import java.util.Date;

/**
 * @author Michael Zurmühle
 */
public interface IPlayerMove {
    Date getTime();
    IPlayer getPlayer();
    IMove getMove();
}
