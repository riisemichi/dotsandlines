package Logic;

import java.awt.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Objects;


/**
 * @author Tamara Toma
 */

public class Move implements IMove, Serializable {
	private Point p1;
	private Point p2;

	protected Move(Point p1, Point p2) {
		// p1 has to be the point closer to the top right.

		if(p1.x <= p2.x && p1.y <= p2.y) {
			this.p1 = p1;
			this.p2 = p2;
		}
		else
		{
			this.p1 = p2;
			this.p2 = p1;
		}

	}

	public Point getP1() {
		return p1;
	}

	public Point getP2() {
		return p2;
	}

	@Override
	public boolean isHorizontalLine() {
		return p1.y == p2.y;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + this.p1.hashCode();
		hash+= 31 * hash + this.p2.hashCode();
		return hash;

	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (obj.getClass() != this.getClass()) {
			return false;
		}
		
		return Objects.equals(this.p1,  ((Move)obj).p1)&& Objects.equals(this.p2, ((Move)obj).p2);
	}
}
