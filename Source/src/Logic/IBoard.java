package Logic;

/**
 * @author Michael Zurmühle
 */
public interface IBoard {
    int getHeight();
    int getWidth();
    IPlayerMove[] getPlayerMoves();
    IBox[] getBoxes();

}
