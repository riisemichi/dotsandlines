package Logic;

import java.awt.Color;

import Logic.Events.IGameLogicListener;

/**
 * @author Michael Zurmühle
 */
public interface IGameLogic {
	/**
	 * @return The game state.
	 */
	GameState getState();

	/**
	 * @return the Winner of the game.
	 */
	IPlayer getWinner();

	/**
	 * @return the Board
	 */
	IBoard getBoard();

	/**
	 * Sets the height and with of the board.
	 * 
	 * @param width
	 *            of the board.
	 * @param height
	 *            of the board.
	 */
	void setBoardSize(int width, int height);

	/**
	 * Start a new game. (only if possible).
	 */
	void startGame();

	/**
	 * Add a player move.
	 * 
	 * @param playerMove
	 *            the move of a player.
	 */
	void addMove(IPlayerMove playerMove);

	/**
	 * @param move
	 *            a move
	 * @return true if valid, false if invalid.
	 */
	boolean isMoveValid(IMove move);

	/**
	 * Return the player who has to do the next move.
	 * @return
	 */
	IPlayer getNextPlayer();

	/**
	 * Get the points which a player has in the currently ongoing game.
	 * 
	 * @param player
	 *            of which the points are asked.
	 * @return amount of points.
	 */
	int getPointsOfPlayer(IPlayer player);

	/**
	 * Cancel the currently ongoing game.
	 */
	void cancelGame();

	ILocalPlayer getNewLocalPlayer(String name, Color color);

	INetworkPlayer getNewNetworkPlayer(String name, Color color);

	/**
	 * Register a listener to recieve Event notifications.
	 * 
	 * @param listener
	 *            the listener that wants to recieve event notifications.
	 */
	void registerGameLogicEvents(IGameLogicListener listener);

	/**
	 * @return the list of IPlayers
	 */
	IPlayer[] getPlayers();

	/**
	 * Sets a player as Automated Intelligence player.
	 * 
	 * @param aiPlayer
	 */
	void setAIPlayer(ILocalPlayer aiPlayer);

	/**
	 * Reset the game. Removes the players, and all the moves.
	 */
	void reset();
}
