package Logic;


/**
 * @author Tamara Toma
 */

public interface INetworkPlayer {

	/**
	 * @param move make a move.
	 */
	void move(IMove move);
}
