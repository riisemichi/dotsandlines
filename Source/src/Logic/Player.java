package Logic;

import java.awt.*;
import java.util.Objects;

/**
 * @author Tamara Toma
 */

class Player implements INetworkPlayer, ILocalPlayer, IPlayer {
	private Color color;
	private String name;
	private GameLogic logic;

	public Player(GameLogic logic, String name, Color color) {
		this.name = name;
		this.color = color;
		this.logic = logic;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void move(IMove move) {
		if (this.equals(this.logic.getNextPlayer())) {
			this.logic.addMove(new PlayerMove(this, move));
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (obj.getClass() != this.getClass()) {
			return false;
		}

		return Objects.equals(this.color, ((Player) obj).color) && Objects.equals(this.name, ((Player) obj).name);

	}
}
