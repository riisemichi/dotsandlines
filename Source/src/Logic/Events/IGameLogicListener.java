package Logic.Events;

import Logic.IPlayerMove;

/**
 * Created by michi on 11.05.15.
 */
public interface IGameLogicListener
{
    void boardChanged();
    void gameStateChanged();
    void nextPlayerChanged();
    void moveDone(IPlayerMove playerMove);
}
