package Logic;

import java.awt.*;


/**
 * @author Michael Zurmühle
 */

public interface IPlayer {
	/**
	 * @return the color of the player.
	 */
	Color getColor();

	/**
	 * @return the name of the player.
	 */
	String getName();
}
