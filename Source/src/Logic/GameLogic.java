package Logic;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import Logic.Events.IGameLogicListener;

/**
 * @author Tamara Toma, Michi Zurmühle
 */

public class GameLogic implements IGameLogic {

	private GameState state;
	private Board board;
	private List<Player> players;
	private IGameLogicListener listener;
	private IPlayer nextPlayer;

	/**
	 * GameLogic constructor.
	 */
	public GameLogic() {
		state = GameState.NotReady;
		board = new Board();
		this.players = new ArrayList<>();
	}

	/**
	 * This method starts the game.
	 */
	public void startGame() {
		this.setState(GameState.Started);
		this.nextPlayer = this.players.get(0);
	}

	/**
	 * This method cancel
	 */
	public void cancelGame() {
		if (this.state != GameState.Cancelled) {
			this.state = GameState.Cancelled;
		}
	}

	/**
	 * This method checks if the game is ready.
	 */
	private void checkStateReady() {
		if ((this.getState() == GameState.NotReady) || (this.getState() == GameState.Cancelled) || (this.getState() == GameState.Ended)) {
			if (this.getBoard() != null && this.getBoard().getHeight() > 0 && this.getBoard().getHeight() > 0 && this.players != null && this.players.size() >= 2) {
				this.setState(GameState.Ready);
			}
		}
	}

	/**
	 * This method checks the state and sets it to ended.
	 */
	private void checkAndSetGameStateToEnded() {
		if (this.getBoard().getBoxes().length >= this.getBoard().getHeight() * this.getBoard().getWidth()) { // All boxes are filled, game ended.
			this.setState(GameState.Ended);
		}
	}

	/**
	 * This method returns the winner.
	 * 
	 * @return IPlayer
	 */
	@Override
	public IPlayer getWinner() {
		IPlayer player = null;
		int points0 = this.getPointsOfPlayer(this.getPlayers()[0]);
		int points1 = this.getPointsOfPlayer(this.getPlayers()[1]);
		if (points0 > points1) {
			player = this.getPlayers()[0];
		} else if (points0 < points1) {
			player = this.getPlayers()[1];
		}
		return player;
	}

	/**
	 * This method sets the board size.
	 */
	@Override
	public void setBoardSize(int width, int height) {
		if (!(this.state == GameState.Started)) {
			this.board.setHeight(height);
			this.board.setWidth(width);
			this.checkStateReady();
		} else {
			// TODO: throw Exception
			// "Can't change board size when game already started."
		}
	}

	/**
	 * This method adds a player move.
	 */
	@Override
	public void addMove(IPlayerMove playerMove) {

		if (isMoveValid(playerMove.getMove())) {
			// make the move
			boolean boxClosed = this.board.addPlayerMove(playerMove);
			this.notifyBoardChanged();

			if (!boxClosed) {
				this.changeNextPlayer();
			}

			// Check if the game ended.
			checkAndSetGameStateToEnded();

			if(this.listener != null) {
				this.listener.nextPlayerChanged();
				this.listener.moveDone(playerMove);
			}
		}
	}

	/**
	 * This method checks if the move is valid.
	 * 
	 * @return boolean
	 */
	@Override
	public boolean isMoveValid(IMove move) {
		// Check if the move is just one line.
		int difference = move.getP2().x - move.getP1().x + move.getP2().y - move.getP1().y;
		if (difference != 1) {
			return false;
		}

		// check if move is on the board.
		if (!((move.getP1().x <= board.getWidth() && move.getP1().x >= 0) && (move.getP2().x <= board.getWidth() && move.getP2().x >= 0)
				&& (move.getP1().y <= board.getHeight() && move.getP1().y >= 0) && (move.getP2().y <= board.getHeight() && move.getP2().y >= 0))) {
			return false;
		}

		boolean any = false;
		for (IPlayerMove pmove : this.board.getPlayerMoves()) {
			if (pmove.getMove().equals(move)) {
				any = true;
				break;
			}
		}
		return !any;
	}

	@Override
	public void registerGameLogicEvents(IGameLogicListener listener) {
		this.listener = listener;

	}

	@Override
	public IPlayer[] getPlayers() {
		return this.players.toArray(new IPlayer[this.players.size()]);
	}

	/**
	 * This method resets the board.
	 */
	@Override
	public void reset() {
		this.board = new Board();
		this.players.clear();
		this.setState(GameState.NotReady);
	}

	@Override
	public void setAIPlayer(ILocalPlayer aiPlayer) {
		// TODO implement AI...
	}

	/**
	 * Returns the points from the player.
	 * 
	 * @return int
	 */
	@Override
	public int getPointsOfPlayer(IPlayer player) {
		int points = 0;
		for (IBox box : this.getBoard().getBoxes()) {
			if (box.getPlayer().equals(player)) {
				points++;
			}
		}
		return points;
	}

	/**
	 * Returns the local player.
	 * 
	 * @return ILocalPlayer
	 */
	@Override
	public ILocalPlayer getNewLocalPlayer(String name, Color color) {
		Player player = new Player(this, name, color);
		addPlayerToList(player);
		return player;
	}

	/**
	 * Returns the network player.
	 * 
	 * @return INetworkPlayer
	 */
	@Override
	public INetworkPlayer getNewNetworkPlayer(String name, Color color) {
		Player player = new Player(this, name, color);
		addPlayerToList(player);
		return player;
	}

	/**
	 * Adds a new player to the list.
	 * 
	 * @param player
	 */
	private void addPlayerToList(Player player) {
		this.players.add(player);
		this.checkStateReady();
	}

	/**
	 * Changes the player during the game.
	 */
	private void changeNextPlayer() {
		if (this.nextPlayer == this.players.get(0)) {
			this.nextPlayer = this.players.get(1);
		} else {
			this.nextPlayer = this.players.get(0);
		}
	}

	/**
	 * Returns the next player.
	 */
	public IPlayer getNextPlayer() {
		return nextPlayer;
	}

	/**
	 * Notifiys when the board has changed.
	 */
	private void notifyBoardChanged() {
		if (this.listener != null) {
			this.listener.boardChanged();
		}
	}

	/**
	 * Notifiys when the state has changed.
	 */
	private void notifyGameStateChanged() {
		if (this.listener != null) {
			this.listener.gameStateChanged();
		}
	}

	/**
	 * Returns the game state.
	 * 
	 * @return GameState
	 */
	public GameState getState() {
		return state;
	}

	/**
	 * Sets the gamestate.
	 * 
	 * @param state
	 */
	private void setState(GameState state) {
		this.state = state;
		this.notifyGameStateChanged();
	}

	/**
	 * This method returns the board.
	 * 
	 * @return IBoard
	 */
	@Override
	public IBoard getBoard() {
		return this.board;
	}
}
