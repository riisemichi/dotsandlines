package Logic;

import java.awt.*;

/**
 * @author Michael Zurmühle
 */
public class Box implements IBox {
    private IPlayer player;
    private Point point;

    public Box(IPlayer player, Point point) {
        this.player = player;
        this.point = point;
    }

    /**
     * @return The player that owns this box.
     */
    @Override
    public IPlayer getPlayer() {
        return this.player;
    }

    /**
     * @return The position of the box.
     * The position is equal to the point on the top left of the box.
     *
     * 00    01   02
     * +----+----+
     * | 00 | 01 |
     * +----+----+
     * 10    11   12
     */
    @Override
    public Point getPoint() {
        return this.point;
    }
}
