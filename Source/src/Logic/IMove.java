package Logic;

import java.awt.*;

/**
 * @author Tamara Toma
 */

public interface IMove {
	/**
	 * 
	 * @return First Point (x,y).
	 */
	Point getP1();

	/**
	 * 
	 * @return Second Point (x,y).
	 */
	Point getP2();

	boolean isHorizontalLine();
}
